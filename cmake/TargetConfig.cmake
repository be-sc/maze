function(config_target target_name)
    target_cxx_standard(${target_name} PUBLIC 17)
    enable_strict_cxx_checks(${target_name})
endfunction()
