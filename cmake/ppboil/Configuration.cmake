#[============================================================[.rst:
:Author: Bernd Schöler <besc@here-be-braces.com>
:URL: https://gitlab.com/be-sc/ppboil
:Version: 5.0.0
:Copyright: CC0/public domain

Provides functions for project and target configuration.
#]============================================================]


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Detects if the current *CMakeLists.txt* has a parent, i.e. if it is used as a part of a larger project. ::

    detect_subproject([<out_varname>])

If the ``<out_varname>`` is used it specifies the name of the output variable. If the function is called without any parameters the output variable is called ``IS_SUBPROJECT``.

Defines the output variable as ``TRUE`` if the current *CMakeLists.txt* is a subproject, ``FALSE`` otherwise.
#]=]
function(detect_subproject)
    if (ARGC GREATER 0)
        set(out_varname ${ARGV0})
    else()
        set(out_varname IS_SUBPROJECT)
    endif()

    get_directory_property(parent_dir PARENT_DIRECTORY)

    if (parent_dir)
        set(${out_varname} TRUE PARENT_SCOPE)
    else()
        set(${out_varname} FALSE PARENT_SCOPE)
    endif()
endfunction()



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Performs C++ and C compiler detection. This means:

* Detailed detection for the “big 3”: Clang, GCC, MSVC.
* Compilers that openly simulate GCC or MSVC via the ``CMAKE_<lang>_SIMULATE_ID`` variable are recognized as GCC compatible or MSVC compatible.
* Also recognizes the Intel compiler as GCC compatible on non-Windows platforms.

Usage::

    detect_cxx_and_c_compilers()

Output variables for C++, set to either ``TRUE`` or ``FALSE``:

``CXX_GCC_COMPAT``
    True if the compiler is GCC, or Clang in GCC-compatible mode, or another compiler reported as GCC compatible.
``CXX_GCC``
    True if the compiler is the real GCC.
``CXX_CLANG``
    True if the compiler is Clang, no matter in which mode.
``CXX_MSVC_COMPAT``
    True if the compiler is MSVC, or Clang in MSVC-compatible command line mode (clang-cl), or
    another compiler reported as MSVC compatible.
``CXX_MSVC``
    True if the compiler is the real MSVC.
``CXX_COMPILER_DETECTED``
    True if any C++ compiler was detected, no matter which one.
``CXX_COMPILER_PP_DEFINES``
    A list of preprocessor macro defines that can be used with ``target_compile_definitions()`` and accessed in the source code with ``#ifdef``. A macro is defined without a value for each of the above output variables set to ``TRUE``. For the other output variables no defines are created. The macro names are prefixed with ``PPBOIL_``. For example, if ``CXX_GCC`` is ``TRUE`` a macro ``PPBOIL_CXX_GCC`` is defined.

For C the same set of variables is defined with the prefix ``C_`` instead of ``CXX_``.

Calling this function multiple times is valid. For an already detected compiler the detection does not run again. Its variables remain unchanged.
#]=]
function(detect_cxx_and_c_compilers)
    if (NOT DEFINED PROJECT_NAME)
        message(FATAL_ERROR "detect_cxx_and_c_compilers() must be called after project().")
    endif()

    if (CMAKE_CXX_COMPILER_ID)
        ppboil_detect_c_or_cxx_compiler(CXX)
    endif()

    if (CMAKE_C_COMPILER_ID)
        ppboil_detect_c_or_cxx_compiler(C)
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Checks if a C++ compiler was detected with ``detect_cxx_and_c_compilers()`` and aborts with a ``FATAL_ERROR`` if not. ::

    check_cxx_compiler_detected()
#]=]
macro(check_cxx_compiler_detected)
    if (NOT CXX_COMPILER_DETECTED)
        message(FATAL_ERROR "Unclear C++ compiler. Call detect_cxx_and_c_compilers() first.")
    endif()
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Checks if a C compiler was detected with ``detect_cxx_and_c_compilers()`` and aborts with a ``FATAL_ERROR`` if not. ::

    check_c_compiler_detected()
#]=]
macro(check_c_compiler_detected)
    if (NOT C_COMPILER_DETECTED)
        message(FATAL_ERROR "Unclear C compiler. Call detect_cxx_and_c_compilers() first.")
    endif()
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Sets the required version of the C++ standard for a target. ::

    target_cxx_standard(<target_name> {PUBLIC|PRIVATE|INTERFACE} <version> [EXTENSIONS])

Parameters:

``{PUBLIC|PRIVATE|INTERFACE}``
    the normal CMake scopes
``version``
    Accepted values are the same as for CMake’s ``CXX_STANDARD`` variable or the ``cxx_std_`` compile feature.
``EXTENSIONS``
    If given compiler extensions are allowed, otherwise they are prohibited.

The function handles both the classic triple of target properties (``CXX_STANDARD``, ``CXX_STANDARD_REQUIRED``, ``CXX_EXTENSIONS``) and the ``cxx_std_<version>`` compile feature, provided the CMake version supports it. The compile feature supports all scopes. Accordingly it is always configured for the given scope. The properties triple is private to the target. Following the usual CMake scope logic it is configured if ``PUBLIC`` or ``PRIVATE`` is given as a scope, but not touched for ``INTERFACE``.

Also the function takes some idiosyncrasies of MSVC++ into account:

* It always enables the alternative operator names mandated in [lex.digraph] in the standard, such as *not* instead of *!*. This is done either by force including the *iso646.h* header or by setting the */permissive-* option, depending on which is more appropriate.
* If the ``EXTENSIONS`` argument is not used and the compiler is at least MSVC++ 2017 it sets the */permissive-* switch to put the compiler into standard compliant mode.
#]=]
function(target_cxx_standard target_name scope version)
    check_cxx_compiler_detected()

    if ("${scope}" STREQUAL "PUBLIC" OR "${scope}" STREQUAL "PRIVATE")
        set(scope_is_pub_or_priv TRUE)
        set(scope_is_intf FALSE)
    elseif ("${scope}" STREQUAL "INTERFACE")
        set(scope_is_pub_or_priv FALSE)
        set(scope_is_intf TRUE)
    endif()

    if (NOT (scope_is_pub_or_priv OR scope_is_intf))
        message(
            FATAL_ERROR
            "Unknown scope: ${scope}; expected one of: PUBLIC PRIVATE INTERFACE "
            "[target: ${target_name}]"
        )
    endif()

    if (ARGC GREATER 3)
        if (NOT "${ARGV3}" STREQUAL "EXTENSIONS")
            message(FATAL_ERROR "Unknown option: ${ARGV2} [target: ${target_name}]")
        endif()
        set(allow_extensions ON)
    else()
        set(allow_extensions OFF)
    endif()

    if (scope_is_pub_or_priv)
        set_target_properties(${target_name} PROPERTIES
            CXX_STANDARD ${version}
            CXX_STANDARD_REQUIRED ON
            CXX_EXTENSIONS ${allow_extensions}
        )
    endif()

    if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.8")
        target_compile_features(${target_name} ${scope} cxx_std_${version})
    endif()

    if (CXX_MSVC)
        # MSVC special logic does two things. 1) It mimics CXX_EXTENSIONS as far as possible.
        # 2) It ensures support for the alternative operator names defined in [lex.digraph]
        # in the standard – e.g. *not* instead of *!*. (1) is only possible in a useful way with
        # the /permissive- switch available since Visual Studio 2017 (compiler version 19.10).
        # (2) can be achieved either through /permissive- or through force-including the iso646.h
        # header.
        if (NOT allow_extensions AND ${MSVC_VERSION} GREATER_EQUAL 1910)
            target_compile_options(${target_name} ${scope} /permissive-)
        else()
            target_compile_options(${target_name} ${scope} /FIiso646.h)
        endif()
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Sets the required version of the C standard for a target. ::

    target_c_standard(<target_name> {PUBLIC|PRIVATE|INTERFACE} <version> [EXTENSIONS])

Parameters:

``{PUBLIC|PRIVATE|INTERFACE}``
    the normal CMake scopes
``version``
    Accepted values are the same as for CMake’s ``C_STANDARD`` variable or the ``c_std_`` compile feature.
``EXTENSIONS``
    If given compiler extensions are allowed, otherwise they are prohibited.

The function handles both the classic triple of target properties (``C_STANDARD``, ``C_STANDARD_REQUIRED``, ``C_EXTENSIONS``) and the ``c_std_<version>`` compile feature, provided the CMake version supports it. The compile feature supports all scopes. Accordingly it is always configured for the given scope. The properties triple is private to the target. Following the usual CMake scope logic it is configured if ``PUBLIC`` or ``PRIVATE`` is given as a scope, but not touched for ``INTERFACE``.
#]=]
function(target_c_standard target_name scope version)
    check_c_compiler_detected()

    if ("${scope}" STREQUAL "PUBLIC" OR "${scope}" STREQUAL "PRIVATE")
        set(scope_is_pub_or_priv TRUE)
        set(scope_is_intf FALSE)
    elseif ("${scope}" STREQUAL "INTERFACE")
        set(scope_is_pub_or_priv FALSE)
        set(scope_is_intf TRUE)
    endif()

    if (NOT (scope_is_pub_or_priv OR scope_is_intf))
        message(
            FATAL_ERROR
            "Unknown scope: ${scope}; expected one of: PUBLIC PRIVATE INTERFACE "
            "[target: ${target_name}]"
        )
    endif()

    if (ARGC GREATER 3)
        if (NOT "${ARGV3}" STREQUAL "EXTENSIONS")
            message(FATAL_ERROR "Unknown option: ${ARGV2} [target: ${target_name}]")
        endif()
        set(allow_extensions ON)
    else()
        set(allow_extensions OFF)
    endif()

    if (scope_is_pub_or_priv)
        set_target_properties(${target_name} PROPERTIES
            C_STANDARD ${version}
            C_STANDARD_REQUIRED ON
            C_EXTENSIONS ${allow_extensions}
        )
    endif()

    if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.8")
        target_compile_features(${target_name} ${scope} c_std_${version})
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
if (CMAKE_VERSION VERSION_LESS "3.12")
# Keep indentation as it is now. Otherwise readme generation breaks!
#[=[.rst:
Safely appends link flags to the existing link flags of the given target. ::

    target_link_flags(<target_name> <flag1> ...)

The ``LINK_FLAGS`` target property is a space separated string. That does not fit CMake’s list handling semantics and makes appending by hand awkward and error prone. Also, we have ``target_compile_options()`` and friends. Having a similar function for the linker just makes sense.
#]=]
    function(target_link_flags target_name)
        # LINK_FLAGS is a space separated string. Joining and quoting as below is mandatory.
        # loop instead of string(REPLACE) to handle values with semicolons correctly
        foreach(value ${ARGN})
            string(APPEND flags " ${value}")
        endforeach()

        set_property(TARGET ${target_name} APPEND_STRING PROPERTY LINK_FLAGS " ${flags}")
    endfunction()
else()
    function(target_link_flags target_name)
        list(JOIN ARGN " " flags)
        set_property(TARGET ${target_name} APPEND_STRING PROPERTY LINK_FLAGS " ${flags}")
    endfunction()
endif()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Sets the symbol visibility for C++ and C targets. ::

    target_lib_visibility(<target_name> <visibility> <inline_visibility>)

If you also want to create a header file with the usual set of export/API macros, consider using ``configure_library_api()`` from the ``LibraryApi`` module instead.
#]=]
function(target_lib_visibility target_name visi inline_visi)
    set_target_properties(${target_name} PROPERTIES
        CXX_VISIBILITY_PRESET ${visi}
        C_VISIBILITY_PRESET ${visi}
        VISIBILITY_INLINES_HIDDEN ${inline_visi}
    )
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Enables strict ``PRIVATE`` C++ compiler warnings and turns some warnings into errors. ::

    enable_strict_cxx_checks(<target_name>)

If you aim at clean, modern C++ this or something similar is probably what you want.
#]=]
function(enable_strict_cxx_checks target_name)
    #[[
    Sets of square brackets [][] contain the compiler version the flag was introduced: GCC in the
    first bracket, Clang in the second. Oldest versions documentation is available for: GCC 2.95,
    Clang 3.2. Flags with those versions may have been introduced even earlier.

    Links to compiler warning flags documentation

    GCC
      replace `x.y.z` with the GCC version number, for latest docs remove the `gcc-x.y.z`
      directory entirely.
      https://gcc.gnu.org/onlinedocs/gcc-x.y.z/gcc/Warning-Options.html
      https://gcc.gnu.org/onlinedocs/gcc-x.y.z/gcc/C_002b_002b-Dialect-Options.html

    Clang
      replace `x.y.z` with the Clang version number
        https://releases.llvm.org/x.y.z/tools/clang/docs/DiagnosticsReference.html
      for docs of current development version
        https://clang.llvm.org/docs/DiagnosticsReference.html

    warning flags collection for GCC and Clang
      https://github.com/Barro/compiler-warnings

    MSVC
      https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warnings-c4000-c5999
      https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warnings-by-compiler-version
    #]]
    check_cxx_compiler_detected()

    if (CXX_GCC)
        list(APPEND flags
            # GCC older than 4.8 does not know -Wpedantic, but all versions know -pedantic
            # -Wextra was introduced in GCC 3.4
            -Wall -Wextra -pedantic

            -Wold-style-cast     # C-style cast (in Clang: same, see below)                                     [2.95] [3.2]
            -Woverloaded-virtual # func decl hides virtual funcs from base class (in Clang: part of -Wall)      [2.95] [3.2]
            -Wctor-dtor-privacy  # class seems unusable b/c not constructible (in Clang: GCC compat, no effect) [2.95] [3.2]
        )

        if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "4.0")
            list(APPEND flags
                # -Werror=<flag> was introduced in GCC 4.0
                # in Clang: same, see below
                -Werror=reorder       # member order mismatch class vs. ctor init list   [4.0] [3.2]
                -Werror=return-type   # no return type or no return in non-void function [4.0] [3.2]
                -Werror=write-strings # assigning string literal to a non-const char*    [4.0] [3.2]
            )
        endif()

        if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "4.7")
            list(APPEND flags
                # in Clang: same, see below
                -Wzero-as-null-pointer-constant  # `0` used as null pointer constant     [4.7] [6]
                -Werror=delete-non-virtual-dtor  # virtual functions but no virtual dtor [4.7] [3.2]
            )
        endif()

        if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "5.0")
            list(APPEND flags
                # force `override` keyword (in Clang: inconsistent-missing-override)     [5] [-]
                -Werror=suggest-override
            )
        endif()

        if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "8.0")
            list(APPEND flags
                # catch handlers not catching exceptions of class type by reference      [8] [-]
                # in Clang: not available
                -Wcatch-value=2
            )
        endif()

        if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "10.0")
            list(APPEND flags
                # inconsistent use of class/struct for same type (in Clang: part of -Wall) [10] [3.2]
                -Wmismatched-tags
            )
        endif()

    elseif (CXX_CLANG)
        if (CXX_MSVC_COMPAT)
            # A few things to remember for clang-cl:
            # + maps /Wall to -Weverything -> not what we want
            # + maps /W4 to -Wall -Wextra -> that’s what we need
            # + Starting options with a slash is not recommended: mistaken for a file path in
            #   some situations. Starting options with a dash is always safe.
            list(APPEND flags -W4 -Wpedantic)
        else()
            list(APPEND flags -Wall -Wextra -Wpedantic)
        endif()

        # For all options below clang and clang-cl behave the same.

        list(APPEND flags
            # in GCC: same, see above
            -Wold-style-cast        # C-style cast, e.g. (int)variable                         [2.95] [3.2]
            -Werror=delete-non-virtual-dtor  # virtual functions but no virtual dtor           [4.7]  [3.2]
            -Werror=reorder         # member order mismatch class vs. ctor init list           [4.0]  [3.2]
            -Werror=return-type     # no return type or no return in non-void function         [4.0]  [3.2]
            -Werror=write-strings   # assigning string literal to a non-const char*            [4.0]  [3.2]
        )

        if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "4.0")
            list(APPEND flags
                -Wassign-enum  # integer constant not in range of enumerated type (in GCC: not available)                  [-] [4]
                -Werror=inconsistent-missing-override       # force `override` for member funcs (in GCC: suggest-override) [-] [4]
                -Winconsistent-missing-destructor-override  # force `override` for dtors (in GCC: not available)           [-] [4]
            )
        endif()

        if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "6.0")
            list(APPEND flags
                -Wzero-as-null-pointer-constant  # `0` used as null pointer constant (in GCC: same, see above)  [4.7] [6]
            )
        endif()

    elseif(CXX_GCC_COMPAT)
        # Compiler is some GCC compatible compiler other than Clang or the real GCC. Assumptions
        # about option support and behaviour when encountering unknown options are tricky. We’ll
        # only assume that the most basic options are available.
        list(APPEND flags -Wall -Wextra)

    elseif(CXX_MSVC_COMPAT)
        # Compiler is the real MSVC or a compatible compiler other than Clang.
        list(APPEND flags
            /W3

            # at least since VS 2002
            /w34053  # one void operand for '?:'
            /w34062  # enumerator 'identifier' in switch of enum 'enumeration' is not handled
            /w34100  # unreferenced formal parameter
            /w34127  # conditional expression is constant
            /w34189  # local variable is initialized but not referenced
            /w34263  # func decl hides virtual funcs from base class
            /w34295  # char array is too small to include a terminating null character
            /w34296  # 'operator' : expression is always true/false
            /w34389  # 'operator' : signed/unsigned mismatch

            # since VS 2005
            /w34365  # conversion from 'type1' to 'type2', signed/unsigned mismatch

            # since VS 2012
            /w34471  # forward declaration of an unscoped enumeration must have an underlying type
        )
    endif()

    target_compile_options(${target_name} PRIVATE ${flags})
endfunction()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# implementation details

macro(ppboil_detect_c_or_cxx_compiler lang)
    if (NOT ${lang}_COMPILER_DETECTED)
        set(${lang}_COMPILER_DETECTED TRUE PARENT_SCOPE)
        set(${lang}_GCC_COMPAT FALSE PARENT_SCOPE)
        set(${lang}_GCC FALSE PARENT_SCOPE)
        set(${lang}_CLANG FALSE PARENT_SCOPE)
        set(${lang}_MSVC_COMPAT FALSE PARENT_SCOPE)
        set(${lang}_MSVC FALSE PARENT_SCOPE)
        set(${lang}_COMPILER_PP_DEFINES "PPBOIL_${lang}_COMPILER_DETECTED")

        if (CMAKE_${lang}_COMPILER_ID STREQUAL "GNU")
            set(${lang}_GCC TRUE PARENT_SCOPE)
            set(${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND ${lang}_COMPILER_PP_DEFINES
                "PPBOIL_${lang}_GCC"
                "PPBOIL_${lang}_GCC_COMPAT"
            )

        elseif (CMAKE_${lang}_COMPILER_ID MATCHES "Clang$") # need fuzzy match to catch Apple Clang
            set(${lang}_CLANG TRUE PARENT_SCOPE)
            list(APPEND ${lang}_COMPILER_PP_DEFINES "PPBOIL_${lang}_CLANG")

            if (CMAKE_${lang}_SIMULATE_ID STREQUAL "MSVC")  # catch clang-cl
                set(${lang}_MSVC_COMPAT TRUE PARENT_SCOPE)
                list(APPEND ${lang}_COMPILER_PP_DEFINES "PPBOIL_${lang}_MSVC_COMPAT")
            else()
                set(${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
                list(APPEND ${lang}_COMPILER_PP_DEFINES "PPBOIL_${lang}_GCC_COMPAT")
            endif()

        elseif (CMAKE_${lang}_COMPILER_ID STREQUAL "MSVC")
            set(${lang}_MSVC TRUE PARENT_SCOPE)
            set(${lang}_MSVC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND ${lang}_COMPILER_PP_DEFINES
                "PPBOIL_${lang}_MSVC"
                "PPBOIL_${lang}_MSVC_COMPAT"
            )

        elseif (NOT WIN32 AND CMAKE_${lang}_COMPILER_ID STREQUAL "Intel")
            set(${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND ${lang}_COMPILER_PP_DEFINES "PPBOIL_${lang}_GCC_COMPAT")
        endif()


        if (CMAKE_${lang}_SIMULATE_ID STREQUAL "GNU")
            set(${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND ${lang}_COMPILER_PP_DEFINES "PPBOIL_${lang}_GCC_COMPAT")
        elseif (CMAKE_${lang}_SIMULATE_ID STREQUAL "MSVC")
            set(${lang}_MSVC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND ${lang}_COMPILER_PP_DEFINES "PPBOIL_${lang}_MSVC_COMPAT")
        endif()

        list(REMOVE_DUPLICATES ${lang}_COMPILER_PP_DEFINES)
        set(${lang}_COMPILER_PP_DEFINES ${${lang}_COMPILER_PP_DEFINES} PARENT_SCOPE)
    endif()
endmacro()
