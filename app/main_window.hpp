#pragma once

#include "growing_tree_config_widget.hpp"
#include "paint_maze.hpp"
#include <maze/field.hpp>
#include <maze/pointer_wrappers.hpp>
#include <QGroupBox>
#include <QLabel>
#include <QMainWindow>
#include <QSettings>
#include <QSize>
#include <QStatusBar>
#include <array>

namespace maze
{

class MainWindow final : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();

protected:
    void closeEvent(QCloseEvent* event) override;

private:
    struct MazeConfig
    {
        int field_kind{0};   // index in the combobox; So, sue me!
        QSize size{50, 50};  // number of cells
        bool auto_solve{true};

        void serialize(QSettings& settings);
        void deserialize(QSettings& settings);
    };

    struct CurrentMaze
    {
        Field field{0};
        int field_kind{0};  // index in the combobox; So, sue me!
        QSize size{};
        bool is_solved{false};
    };

    struct FieldFunctions
    {
        QString name;
        FieldFactoryFunc make_field;
        PaintMazeFunc paint_maze;
        PaintSolutionFunc paint_solution;
    };

    void setup_ui_basics();
    void setup_ui_controls_dock();
    QtOwned<QGroupBox*> setup_ui_maze_config_group(QWidget* parent);
    QtOwned<QWidget*> setup_ui_stats_group(QWidget* parent);
    void save_settings();
    void load_window_settings();
    void load_config_settings();

    Q_SLOT void generate_maze();
    void solve_maze(QPixmap& image);
    Q_SLOT void on_solve_clicked();

    static std::array<FieldFunctions, 2> m_field_functions;

    QtOwned<QLabel*> m_canvas{};
    QtOwned<QStatusBar*> m_status_bar{};
    QtOwned<QLabel*> m_gen_status{};
    QtOwned<QLabel*> m_solution_status{};

    QtOwned<QLabel*> m_stats_maze_size{};
    QtOwned<QLabel*> m_stats_maze_dead_ends{};
    QtOwned<QLabel*> m_stats_solution_length{};
    QtOwned<QLabel*> m_stats_solution_wrong_turns{};

    QtOwned<gt::GrowingTreeConfigWidget*> m_gtree_cfg_widget{};
    MazeConfig m_maze_cfg{};
    QSettings m_settings;

    // keeping track of the generated maze for solving it later
    CurrentMaze m_maze{};
};

}  // namespace maze
