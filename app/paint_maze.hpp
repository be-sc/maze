#pragma once

#include <maze/field.hpp>
#include <QPixmap>
#include <QSize>
#include <vector>

namespace maze
{

using PaintMazeFunc = QPixmap (*)(const Field&, QSize);
using PaintSolutionFunc = void (*)(QPixmap&, const std::vector<CellIndex>&, const Field&, QSize);


QPixmap paint_rectangular_maze(const Field& field, QSize cells);
QPixmap paint_straight_hexagon_maze(const Field& field, QSize cells);


void paint_rectangular_solution(
        QPixmap& canvas, const std::vector<CellIndex>& solution, const Field& field, QSize cells);
void paint_straight_hexagon_solution(
        QPixmap& canvas, const std::vector<CellIndex>& solution, const Field& field, QSize cells);

}  // namespace maze
