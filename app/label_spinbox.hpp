#pragma once

#include <maze/pointer_wrappers.hpp>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QString>
#include <QWidget>

namespace maze
{

class LabelSpinBox : public QWidget
{
    Q_OBJECT

public:
    explicit LabelSpinBox(QBoxLayout::Direction dir, QWidget* parent = nullptr);
    explicit LabelSpinBox(
            const QString& title, QBoxLayout::Direction dir, QWidget* parent = nullptr);

    void setTitle(const QString& title) { m_label->setText(title); }
    QString title() const { return m_label->text(); }

    QString cleanText() const { return m_spin_box->cleanText(); }
    int displayIntegerBase() const { return m_spin_box->displayIntegerBase(); }
    int maximum() const { return m_spin_box->maximum(); }
    int minimum() const { return m_spin_box->minimum(); }
    QString prefix() const { return m_spin_box->prefix(); }
    void setDisplayIntegerBase(int base) { m_spin_box->setDisplayIntegerBase(base); }
    void setMaximum(int max) { m_spin_box->setMaximum(max); }
    void setMinimum(int min) { m_spin_box->setMinimum(min); }
    void setPrefix(const QString& prefix) { m_spin_box->setPrefix(prefix); }
    void setRange(int minimum, int maximum) { m_spin_box->setRange(minimum, maximum); }
    void setSingleStep(int val) { m_spin_box->setSingleStep(val); }
    void setStepType(QAbstractSpinBox::StepType stepType) { m_spin_box->setStepType(stepType); }
    void setSuffix(const QString& suffix) { m_spin_box->setSuffix(suffix); }
    Q_SLOT void setValue(int val) { m_spin_box->setValue(val); }
    int singleStep() const { return m_spin_box->singleStep(); }
    QAbstractSpinBox::StepType stepType() const { return m_spin_box->stepType(); }
    QString suffix() const { return m_spin_box->suffix(); }
    int value() const { return m_spin_box->value(); }

Q_SIGNALS:
    void valueChanged(int value);

private:
    QtOwned<QLabel*> m_label;
    QtOwned<QSpinBox*> m_spin_box;
};

}  // namespace maze
