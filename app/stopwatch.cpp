#include "stopwatch.hpp"

#include <QDateTime>

namespace maze
{

Stopwatch Stopwatch::create_and_start()
{
    Stopwatch s;
    s.start();
    return s;
}


void Stopwatch::start()
{
    m_start = QDateTime::currentMSecsSinceEpoch();
    m_stop = 0;
}


Stopwatch::Milliseconds Stopwatch::stop()
{
    if (is_running()) {
        m_stop = QDateTime::currentMSecsSinceEpoch();
    }

    return m_stop - m_start;
}


Stopwatch::Milliseconds Stopwatch::elapsed() const
{
    if (is_running()) {
        return QDateTime::currentMSecsSinceEpoch() - m_start;
    }
    return m_stop - m_start;
}


bool Stopwatch::is_running() const
{
    return m_stop == 0 && m_start != 0;
}

}  // namespace maze
