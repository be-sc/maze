#include "main_window.hpp"

#include "label_spinbox.hpp"
#include "stopwatch.hpp"
#include "title_label.hpp"
#include <maze/solver.hpp>
#include <maze/statistics.hpp>
#include <QApplication>
#include <QCloseEvent>
#include <QComboBox>
#include <QDockWidget>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QScrollArea>
#include <QSpinBox>
#include <QVBoxLayout>

namespace maze
{

namespace
{
// identifiers for the settings file
const QString window_group(QStringLiteral("window"));
const QString geometry_key(QStringLiteral("geometry"));
const QString state_key(QStringLiteral("state"));

const QString current_cfg_group(QStringLiteral("current_config"));
const QString maze_group(QStringLiteral("maze"));
const QString kind_key(QStringLiteral("kind"));
const QString size_key(QStringLiteral("size"));
const QString auto_solve_key(QStringLiteral("auto_solve"));


class MazeScrollArea : public QScrollArea
{
public:
    MazeScrollArea() : QScrollArea(nullptr)
    {
        QPalette white_palette;
        white_palette.setColor(QPalette::Window, Qt::white);

        setAutoFillBackground(true);
        setPalette(white_palette);
        setWidgetResizable(true);
    }

    QtOwned<QLabel*> create_content()
    {
        auto scr_content = new QWidget;
        auto layout = new QVBoxLayout(scr_content);
        auto canvas = new QLabel(scr_content);

        layout->addWidget(canvas);
        layout->addStretch();

        setWidget(scr_content);

        return canvas;
    }
};


QString format_stat(const CountAndPercentage& value)
{
    return QStringLiteral("%1 (%2%)").arg(value.count).arg(value.percentage, 0, 'f', 1);
}

}  // namespace


std::array<MainWindow::FieldFunctions, 2> MainWindow::m_field_functions{{
        {QStringLiteral("rectangular"),
         &make_rectangular_field,
         &paint_rectangular_maze,
         &paint_rectangular_solution},

        {QStringLiteral("straight_hexagonal"),
         &make_straight_hexagon_field,
         &paint_straight_hexagon_maze,
         &paint_straight_hexagon_solution},
}};


MainWindow::MainWindow() : QMainWindow(nullptr)
{
    setWindowTitle(QStringLiteral("Maze 1.0"));
    load_config_settings();

    setup_ui_basics();
    setup_ui_controls_dock();

    load_window_settings();
}


// helper function for ctor
void MainWindow::setup_ui_basics()
{
    resize(1050, 800);

    m_status_bar = new QStatusBar;
    setStatusBar(m_status_bar);

    m_gen_status = new QLabel(m_status_bar);
    m_gen_status->setTextFormat(Qt::PlainText);
    m_status_bar->addPermanentWidget(m_gen_status);

    m_solution_status = new QLabel(m_status_bar);
    m_solution_status->setTextFormat(Qt::PlainText);
    m_status_bar->addPermanentWidget(m_solution_status);

    QtOwned<MazeScrollArea*> scroller = new MazeScrollArea;
    setCentralWidget(scroller);
    m_canvas = scroller->create_content();
}


// helper function for ctor
void MainWindow::setup_ui_controls_dock()
{
    auto dock = new QDockWidget(this);
    dock->setObjectName(QStringLiteral("config_dock"));
    dock->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
    dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    addDockWidget(Qt::LeftDockWidgetArea, dock);

    auto content = new QWidget;
    dock->setWidget(content);

    auto content_layout = new QVBoxLayout(content);

    auto algo_config_group = new QGroupBox(content);
    algo_config_group->setTitle(QStringLiteral("&Algorithm config"));
    auto algo_layout = new QVBoxLayout(algo_config_group);
    m_settings.beginGroup(current_cfg_group);
    m_gtree_cfg_widget = new gt::GrowingTreeConfigWidget(
            gt::GrowingTreeConfig::deserialize(m_settings), algo_config_group);
    m_settings.endGroup();
    algo_layout->addWidget(m_gtree_cfg_widget);

    auto btn_layout = new QHBoxLayout(content);
    btn_layout->setContentsMargins(0, 0, 0, 0);

    auto generate_button = new QPushButton(content);
    btn_layout->addWidget(generate_button);
    generate_button->setText(QStringLiteral("&Generate (F5)"));
    generate_button->setShortcut(Qt::Key_F5);
    generate_button->setDefault(true);
    connect(generate_button, &QPushButton::clicked, this, &MainWindow::generate_maze);

    auto solve_button = new QPushButton(content);
    btn_layout->addWidget(solve_button);
    solve_button->setText(QStringLiteral("&Solve (F6)"));
    solve_button->setShortcut(Qt::Key_F6);
    solve_button->setDefault(false);
    connect(solve_button, &QPushButton::clicked, this, &MainWindow::on_solve_clicked);

    content_layout->addWidget(algo_config_group);
    content_layout->addWidget(setup_ui_maze_config_group(content));
    content_layout->addWidget(setup_ui_stats_group(content));
    content_layout->addStretch();
    content_layout->addLayout(btn_layout);
}

// helper function for ctor
QtOwned<QGroupBox*> MainWindow::setup_ui_maze_config_group(QWidget* parent)
{
    QtOwned<QGroupBox*> group = new QGroupBox(parent);
    group->setTitle(QStringLiteral("&Maze config"));

    QtOwned<QVBoxLayout*> layout = new QVBoxLayout(group);

    QtOwned<QComboBox*> field_kind_combo = new QComboBox(group);
    for (const auto& funcs : m_field_functions) {
        field_kind_combo->addItem(funcs.name);
    }
    field_kind_combo->setCurrentIndex(m_maze_cfg.field_kind);
    connect(field_kind_combo,
            static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this,
            [&](int idx) { m_maze_cfg.field_kind = idx; });


    QtOwned<QHBoxLayout*> dim_layout = new QHBoxLayout(group);

    QtOwned<LabelSpinBox*> width_spinner = new LabelSpinBox(QBoxLayout::TopToBottom, group);
    width_spinner->setTitle(QStringLiteral("width"));
    width_spinner->setMaximum(10000);
    width_spinner->setValue(m_maze_cfg.size.width());
    width_spinner->setSuffix(QStringLiteral(" cells"));
    connect(width_spinner, &LabelSpinBox::valueChanged, this, [&](int w) {
        m_maze_cfg.size.setWidth(w);
    });
    dim_layout->addWidget(width_spinner);

    QtOwned<LabelSpinBox*> height_spinner = new LabelSpinBox(QBoxLayout::TopToBottom, group);
    height_spinner->setTitle(QStringLiteral("height"));
    height_spinner->setMaximum(10000);
    height_spinner->setValue(m_maze_cfg.size.height());
    height_spinner->setSuffix(QStringLiteral(" cells"));
    connect(height_spinner, &LabelSpinBox::valueChanged, this, [&](int h) {
        m_maze_cfg.size.setHeight(h);
    });
    dim_layout->addWidget(height_spinner);

    auto solve_check = new QCheckBox(group);
    solve_check->setText(QStringLiteral("auto sol&ve after generation"));
    solve_check->setChecked(m_maze_cfg.auto_solve);
    connect(solve_check, &QCheckBox::stateChanged, this, [&](int state) {
        m_maze_cfg.auto_solve = state != Qt::Unchecked;
    });

    layout->addWidget(field_kind_combo);
    layout->addLayout(dim_layout);
    layout->addWidget(solve_check);

    return group;
}


QtOwned<QWidget*> MainWindow::setup_ui_stats_group(QWidget* parent)
{
    auto group = new QWidget(parent);
    // group->setTitle(QStringLiteral("Statistics"));

    auto grid = new QGridLayout(group);

    grid->addWidget(new TitleLabel(QStringLiteral("Maze statistics"), group), 0, 0, 1, 2);

    grid->addWidget(new QLabel(QStringLiteral("cell count"), group), 1, 0);
    m_stats_maze_size = new QLabel(group);
    grid->addWidget(m_stats_maze_size, 1, 1);

    grid->addWidget(new QLabel(QStringLiteral("dead ends"), group), 2, 0);
    m_stats_maze_dead_ends = new QLabel(group);
    m_stats_maze_dead_ends->setToolTip(QStringLiteral("% of cell count"));
    grid->addWidget(m_stats_maze_dead_ends, 2, 1);


    grid->setRowMinimumHeight(3, 6);


    grid->addWidget(new TitleLabel(QStringLiteral("Solution statistics"), group), 4, 0, 1, 2);

    grid->addWidget(new QLabel(QStringLiteral("step count"), group), 5, 0);
    m_stats_solution_length = new QLabel(group);
    m_stats_solution_length->setToolTip(QStringLiteral("% of cell count"));
    grid->addWidget(m_stats_solution_length, 5, 1);

    grid->addWidget(new QLabel(QStringLiteral("wrong turns"), group), 6, 0);
    m_stats_solution_wrong_turns = new QLabel(group);
    m_stats_solution_wrong_turns->setToolTip(QStringLiteral("% of step count"));
    grid->addWidget(m_stats_solution_wrong_turns, 6, 1);

    return group;
}


void MainWindow::closeEvent(QCloseEvent* event)
{
    save_settings();
    event->accept();
}


void MainWindow::save_settings()
{
    m_settings.beginGroup(window_group);
    m_settings.setValue(geometry_key, saveGeometry());
    m_settings.setValue(state_key, saveState());
    m_settings.endGroup();

    m_settings.beginGroup(current_cfg_group);
    m_maze_cfg.serialize(m_settings);
    m_gtree_cfg_widget->serialize(m_settings);
    m_settings.endGroup();

    m_settings.sync();
}


void MainWindow::load_window_settings()
{
    m_settings.beginGroup(window_group);
    restoreGeometry(m_settings.value(geometry_key).toByteArray());
    restoreState(m_settings.value(state_key).toByteArray());
    m_settings.endGroup();
}


void MainWindow::load_config_settings()
{
    m_settings.beginGroup(current_cfg_group);
    m_maze_cfg.deserialize(m_settings);
    m_settings.endGroup();
}


void MainWindow::MazeConfig::serialize(QSettings& settings)
{
    settings.beginGroup(maze_group);
    settings.setValue(kind_key, field_kind);
    settings.setValue(size_key, size);
    settings.setValue(auto_solve_key, auto_solve);
    settings.endGroup();
}


void MainWindow::MazeConfig::deserialize(QSettings& settings)
{
    settings.beginGroup(maze_group);

    {
        const auto field_kind_in = settings.value(kind_key);
        if (field_kind_in.canConvert<int>()) {
            field_kind = field_kind_in.value<int>();
        }
    }
    {
        const auto size_in = settings.value(size_key);
        if (size_in.canConvert<QSize>()) {
            size = size_in.value<QSize>();
        }
    }
    {
        const auto solve_in = settings.value(auto_solve_key);
        if (solve_in.canConvert<bool>()) {
            auto_solve = solve_in.value<bool>();
        }
    }

    settings.endGroup();
}


void MainWindow::generate_maze()
{
    // bescbau: Work should really be happening in its own thread!
    m_status_bar->showMessage("working …");
    qApp->processEvents();

    save_settings();

    Q_ASSERT(m_maze_cfg.field_kind >= 0);
    Q_ASSERT(m_maze_cfg.field_kind < static_cast<int>(m_field_functions.size()));

    if (not m_gtree_cfg_widget->validate()) {
        m_status_bar->showMessage("no cell selection or percentages do not add up to 100%", 10000);
        return;
    }

    auto cell_select_strat = m_gtree_cfg_widget->make_cell_select_strat();
    auto nb_select_strat = m_gtree_cfg_widget->make_neighbour_select_strat();

    const auto& funcs = m_field_functions[m_maze_cfg.field_kind];

    auto gen_stopwatch = Stopwatch::create_and_start();
    m_maze.field = funcs.make_field(m_maze_cfg.size.width(), m_maze_cfg.size.height());
    gt::GrowingTreeGenerator gen(
            m_maze.field, std::move(cell_select_strat), std::move(nb_select_strat));
    gen.run();
    gen_stopwatch.stop();

    auto paint_stopwatch = Stopwatch::create_and_start();
    QPixmap image = funcs.paint_maze(m_maze.field, m_maze_cfg.size);
    paint_stopwatch.stop();

    m_maze.field_kind = m_maze_cfg.field_kind;
    m_maze.size = m_maze_cfg.size;
    m_maze.is_solved = false;

    if (m_maze_cfg.auto_solve) {
        solve_maze(image);
    }
    else {
        m_solution_status->setText("");
    }

    m_canvas->setPixmap(image);
    m_canvas->scroll(0, 0);

    const auto stats = analyze_maze(m_maze.field);
    m_stats_maze_size->setText(QString::number(stats.cell_count));
    m_stats_maze_dead_ends->setText(format_stat(stats.dead_ends));

    m_status_bar->clearMessage();
    m_gen_status->setText(QStringLiteral("generate: %1 ms, paint: %2 ms")
                                  .arg(gen_stopwatch.elapsed())
                                  .arg(paint_stopwatch.elapsed()));
}


void MainWindow::solve_maze(QPixmap& image)
{
    auto solve_stopwatch = Stopwatch::create_and_start();
    const auto solution = solve_depth_first(m_maze.field);
    solve_stopwatch.stop();

    auto paint_stopwatch = Stopwatch::create_and_start();
    m_field_functions[m_maze.field_kind].paint_solution(
            image, solution, m_maze.field, m_maze_cfg.size);
    paint_stopwatch.stop();

    m_maze.is_solved = true;

    const auto stats = analyze_solution(m_maze.field, solution);
    m_stats_solution_length->setText(format_stat(stats.steps));
    m_stats_solution_wrong_turns->setText(format_stat(stats.wrong_turns));

    m_solution_status->setText(QStringLiteral("| solve: %1 ms, paint: %2 ms")
                                       .arg(solve_stopwatch.elapsed())
                                       .arg(paint_stopwatch.elapsed()));
}


void MainWindow::on_solve_clicked()
{
    if (m_maze.is_solved || m_maze.field.is_empty()) {
        return;
    }

    QPixmap image = m_canvas->pixmap(Qt::ReturnByValue);
    solve_maze(image);
    m_canvas->setPixmap(image);
}

}  // namespace maze
