#include "growing_tree_config_widget.hpp"

#include "label_spinbox.hpp"
#include "title_label.hpp"
#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>

namespace maze::gt
{

namespace
{
inline QtOwned<QFrame*> make_horizontal_line(QWidget* parent)
{
    auto line = new QFrame(parent);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    return line;
}

template<typename T>
void maybe_read_settings_value(T& dest, const QSettings& settings, const QString& key)
{
    auto val = settings.value(key);

    if (val.canConvert<T>()) {
        dest = val.value<T>();
    }
}

template<typename T>
void maybe_cast_settings_value(T& dest, const QSettings& settings, const QString& key)
{
    auto val = settings.value(key);

    if (val.canConvert<int>()) {
        dest = static_cast<T>(val.value<int>());
    }
}

const QString gtree_group(QStringLiteral("gtree"));
const QString cell_select_array(QStringLiteral("cell_select"));
const QString enabled_key(QStringLiteral("enabled"));
const QString strategy_key(QStringLiteral("strategy"));
const QString weight_key(QStringLiteral("weight"));
const QString weight_mode_key(QStringLiteral("weight_mode"));
const QString neighbour_count_key(QStringLiteral("neighbour_count"));
const QString neighbour_strat_key(QStringLiteral("neighbour_strat"));

struct NamedCellSelectFunc
{
    QString name;
    CellSelectFunc func;
};

std::array<NamedCellSelectFunc, 4> cell_select_funcs{
        {{QStringLiteral("random"), &select_active_at_random},
         {QStringLiteral("newest"), &select_newest_active},
         {QStringLiteral("oldest"), &select_oldest_active},
         {QStringLiteral("middle"), &select_middle_active}}};
}  // namespace

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

GrowingTreeConfig GrowingTreeConfig::deserialize(QSettings& settings)
{
    GrowingTreeConfig cfg;
    settings.beginGroup(gtree_group);

    int select_count = settings.beginReadArray(cell_select_array);
    select_count = std::min(select_count, 3);

    for (int i = 0; i < select_count; ++i) {
        settings.setArrayIndex(i);
        maybe_read_settings_value(cfg.cell_selection[i].enabled, settings, enabled_key);
        maybe_read_settings_value(cfg.cell_selection[i].strategy, settings, strategy_key);
        maybe_read_settings_value(cfg.cell_selection[i].weight, settings, weight_key);
    }
    settings.endArray();

    maybe_cast_settings_value(cfg.weight_mode, settings, weight_mode_key);
    maybe_read_settings_value(cfg.neighbour_count, settings, neighbour_count_key);
    maybe_cast_settings_value(cfg.neighbour_strategy, settings, neighbour_strat_key);

    settings.endGroup();
    return cfg;
}


void GrowingTreeConfigWidget::serialize(QSettings& settings)
{
    settings.beginGroup(gtree_group);

    settings.beginWriteArray(cell_select_array, m_config.cell_selection.size());
    for (unsigned i = 0; i < m_config.cell_selection.size(); ++i) {
        settings.setArrayIndex(i);
        auto& cfg = m_config.cell_selection[i];
        settings.setValue(enabled_key, cfg.enabled);
        settings.setValue(strategy_key, cfg.strategy);
        settings.setValue(weight_key, cfg.weight);
    }
    settings.endArray();

    settings.setValue(weight_mode_key, static_cast<int>(m_config.weight_mode));
    settings.setValue(neighbour_count_key, m_config.neighbour_count);
    settings.setValue(neighbour_strat_key, static_cast<int>(m_config.neighbour_strategy));

    settings.endGroup();
}


bool GrowingTreeConfigWidget::validate() const
{
    return at_least_one_strat_enabled();
}


CellSelectFunctor GrowingTreeConfigWidget::make_cell_select_strat() const
{
    CellSelectCombined strat;

    for (const auto& sel : m_config.cell_selection) {
        if (sel.enabled) {
            strat.add_strat(cell_select_funcs[sel.strategy].func, sel.weight);
        }
    }

    // if only one strat is configured the plain selection function is more efficient
    if (auto only_strat = strat.only_strat(); only_strat) {
        return *only_strat;
    }

    strat.set_mode(m_config.weight_mode);
    return CellSelectFunctor(std::move(strat));
}


NeighbourSelect GrowingTreeConfigWidget::make_neighbour_select_strat() const
{
    return NeighbourSelect(m_config.neighbour_strategy, m_config.neighbour_count);
}


bool GrowingTreeConfigWidget::at_least_one_strat_enabled() const
{
    return m_config.cell_selection[0].enabled || m_config.cell_selection[1].enabled
           || m_config.cell_selection[2].enabled;
}


GrowingTreeConfigWidget::GrowingTreeConfigWidget(GrowingTreeConfig&& config, QWidget* parent)
    : QWidget(parent), m_config(std::move(config))
{
    auto layout = new QVBoxLayout(this);

    if (not at_least_one_strat_enabled()) {
        m_config.cell_selection[0].enabled = true;
    }

    layout->addWidget(new TitleLabel(QStringLiteral("active cell selection"), this));
    layout->addWidget(new ActiveCellSelectionWidget(m_config.cell_selection[0], this));
    layout->addWidget(new ActiveCellSelectionWidget(m_config.cell_selection[1], this));
    layout->addWidget(new ActiveCellSelectionWidget(m_config.cell_selection[2], this));


    auto weight_layout = new QHBoxLayout(this);
    weight_layout->addWidget(new QLabel(QStringLiteral("weight mode"), this));

    auto weight_combo = new QComboBox(this);
    weight_layout->addWidget(weight_combo);
    weight_combo->addItems({QStringLiteral("random"), QStringLiteral("in order")});
    weight_combo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    weight_combo->setCurrentIndex(static_cast<int>(m_config.weight_mode));
    connect(weight_combo,
            static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this,
            [&](int idx) { m_config.weight_mode = static_cast<WeightMode>(idx); });

    layout->addLayout(weight_layout);


    layout->addWidget(make_horizontal_line(this));
    layout->addWidget(new TitleLabel(QStringLiteral("neighbour selection"), this));


    auto nb_layout = new QHBoxLayout(this);
    nb_layout->setContentsMargins(0, 0, 0, 0);

    auto nb_count_spinner =
            new LabelSpinBox(QStringLiteral("up to"), QBoxLayout::LeftToRight, this);
    nb_layout->addWidget(nb_count_spinner);
    nb_count_spinner->setMinimum(1);
    nb_count_spinner->setMaximum(6);
    nb_count_spinner->setValue(m_config.neighbour_count);
    connect(nb_count_spinner, &LabelSpinBox::valueChanged, this, [&](int value) {
        m_config.neighbour_count = value;
    });

    auto nb_combo = new QComboBox(this);
    nb_layout->addWidget(nb_combo);
    nb_combo->addItems(
            {QStringLiteral("random"),
             QStringLiteral("index order (asc)"),
             QStringLiteral("index order (desc)")});
    nb_combo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    nb_combo->setCurrentIndex(static_cast<int>(m_config.neighbour_strategy));
    connect(nb_combo,
            static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this,
            [&](int idx) {
                m_config.neighbour_strategy = static_cast<NeighbourSelectStrategy>(idx);
            });


    layout->addLayout(nb_layout);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


ActiveCellSelectionWidget::ActiveCellSelectionWidget(CellSelectConfig& config, QWidget* parent)
    : QWidget(parent), m_config(config)
{
    auto layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    m_active_check = new QCheckBox(this);
    layout->addWidget(m_active_check);
    m_active_check->setChecked(m_config.enabled);
    connect(m_active_check,
            &QCheckBox::stateChanged,
            this,
            &ActiveCellSelectionWidget::check_state_changed);

    m_strategy_combo = new QComboBox(this);
    layout->addWidget(m_strategy_combo);
    for (const auto& nfunc : cell_select_funcs) {
        m_strategy_combo->addItem(nfunc.name);
    }
    m_strategy_combo->setCurrentIndex(m_config.strategy);
    m_strategy_combo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    connect(m_strategy_combo,
            static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this,
            [&](int idx) { m_config.strategy = idx; });

    m_weight_spinner = new QSpinBox(this);
    layout->addWidget(m_weight_spinner);
    m_weight_spinner->setMinimum(1);
    m_weight_spinner->setMaximum(10000);
    m_weight_spinner->setSingleStep(1);
    m_weight_spinner->setValue(m_config.weight);
    connect(m_weight_spinner,
            static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
            this,
            [&](int value) { m_config.weight = value; });

    update_enabledness();
}


void ActiveCellSelectionWidget::check_state_changed(int state)
{
    m_config.enabled = state != Qt::Unchecked;
    update_enabledness();
}


void ActiveCellSelectionWidget::update_enabledness()
{
    const bool is_enabled = m_config.enabled;
    m_strategy_combo->setEnabled(is_enabled);
    m_weight_spinner->setEnabled(is_enabled);
}

}  // namespace maze::gt
