#include "paint_maze.hpp"

#include <QImage>
#include <QPainter>
#include <QPen>
#include <QPoint>
#include <QRect>
#include <vector>

namespace maze
{

namespace
{
constexpr int corridor_size = 10;
constexpr int rwall_size = 2;
constexpr int cell_size = corridor_size + rwall_size;

// includes the gates
inline void paint_rectangular_border(const Field& field, QSize canvas_size, QPainter& p)
{
    const QPoint top_left{1, 1};
    const QPoint top_right{canvas_size.width() - 1, top_left.y()};
    const QPoint bottom_left{top_left.x(), canvas_size.height() - 1};
    const QPoint bottom_right{top_right.x(), bottom_left.y()};

    if (field.gates()[0].edge == FieldEdge::top) {
        const QPoint seg1_end(rwall_size + (field.gates()[0].wall_index * cell_size), top_left.y());
        const QPoint seg2_start(seg1_end.x() + corridor_size, top_left.y());
        p.drawLine(top_left, seg1_end);
        p.drawLine(seg2_start, top_right);
    }
    else {
        p.drawLine(top_left, top_right);
    }

    if (field.gates()[1].edge == FieldEdge::bottom) {
        const QPoint seg1_end(
                rwall_size + (field.gates()[1].wall_index * cell_size), bottom_left.y());
        const QPoint seg2_start(seg1_end.x() + corridor_size, bottom_left.y());
        p.drawLine(bottom_left, seg1_end);
        p.drawLine(seg2_start, bottom_right);
    }
    else {
        p.drawLine(bottom_left, bottom_right);
    }

    if (field.gates()[0].edge == FieldEdge::left) {
        const QPoint seg1_end(top_left.x(), rwall_size + (field.gates()[0].wall_index * cell_size));
        const QPoint seg2_start(top_left.x(), seg1_end.y() + corridor_size);
        p.drawLine(top_left, seg1_end);
        p.drawLine(seg2_start, bottom_left);
    }
    else {
        p.drawLine(top_left, bottom_left);
    }

    if (field.gates()[1].edge == FieldEdge::right) {
        const QPoint seg1_end(
                top_right.x(), rwall_size + (field.gates()[1].wall_index * cell_size));
        const QPoint seg2_start(top_right.x(), seg1_end.y() + corridor_size);
        p.drawLine(top_right, seg1_end);
        p.drawLine(seg2_start, bottom_right);
    }
    else {
        p.drawLine(top_right, bottom_right);
    }
}
}  // namespace


QPixmap paint_rectangular_maze(const Field& field, QSize cells)
{
    Q_ASSERT((static_cast<size_t>(cells.width()) * cells.height()) == field.cell_count());

    QImage canvas(
            cells.width() * cell_size + rwall_size,
            cells.height() * cell_size + rwall_size,
            QImage::Format_RGB32);
    canvas.fill(Qt::white);

    {
        QPen pen(Qt::black);
        pen.setWidth(rwall_size);
        pen.setCapStyle(Qt::FlatCap);

        QPainter p(&canvas);
        p.setPen(pen);

        paint_rectangular_border(field, canvas.size(), p);

        CellIndex cell_idx = 0;
        const auto last_row = cells.height() - 1;

        for (int row = 0; row < cells.height(); ++row) {
            const int top_y = rwall_size + (row * cell_size);
            const auto last_col = cells.width() - 1;

            for (int col = 0; col < cells.width(); ++col) {
                const QRect cell_rect{rwall_size + (col * cell_size), top_y, cell_size, cell_size};

                const bool draw_right_wall =
                        (col != last_col)
                        && (not field.has_passage_between(cell_idx, cell_idx + 1));
                if (draw_right_wall) {
                    // right wall
                    p.drawLine(
                            cell_rect.right(),
                            cell_rect.top(),
                            cell_rect.right(),
                            cell_rect.bottom() + 1);
                }

                const bool draw_bottom_wall =
                        (row != last_row)
                        && (not field.has_passage_between(cell_idx, cell_idx + cells.width()));
                if (draw_bottom_wall) {
                    // bottom wall
                    p.drawLine(
                            cell_rect.left(),
                            cell_rect.bottom(),
                            cell_rect.right() + 1,
                            cell_rect.bottom());
                }

                ++cell_idx;
            }
        }
    }

    return QPixmap::fromImage(std::move(canvas));
}


namespace
{
constexpr int cell_width = 16;
constexpr int cell_half = cell_width / 2;
constexpr int triangle_height = cell_width / 4;
constexpr int line_height = cell_half + triangle_height;
constexpr int xwall_size = 2;

// includes the gates
inline void paint_staight_hexagon_border(
        const Field& field, QSize cells, QSize canvas_size, QPainter& p)
{
    const bool has_even_row_count = cells.height() % 2 == 0;

    std::vector<QPoint> border_a;
    border_a.reserve(std::max(cells.width(), cells.height()) * 2);

    std::vector<QPoint> border_b;
    border_b.reserve(std::max(cells.width(), cells.height()) * 2);

    {  // top/bottom borders
        const int bottom_y_high = canvas_size.height() - triangle_height - xwall_size + 1;
        const int bottom_y_low = canvas_size.height() - xwall_size + 1;
        const int bottom_x_offset = has_even_row_count ? cell_half : 0;

        border_a.push_back({1, triangle_height});
        border_b.push_back({1 + bottom_x_offset, bottom_y_high});

        for (int col = 0; col < cells.width(); ++col) {
            int x = 1 + (col * cell_width) + cell_half;
            border_a.push_back({x, 1});
            border_a.push_back({x + cell_half, 1 + triangle_height});

            border_b.push_back({bottom_x_offset + x, bottom_y_low});
            border_b.push_back({bottom_x_offset + x + cell_half, bottom_y_high});
        }

        if (field.gates()[0].edge == FieldEdge::top) {
            const auto wall_idx = field.gates()[0].wall_index * 2;
            p.drawPolyline(border_a.data(), wall_idx + 1);
            p.drawPolyline(border_a.data() + wall_idx + 2, border_a.size() - wall_idx - 2);
        }
        else {
            p.drawPolyline(border_a.data(), border_a.size());
        }

        if (field.gates()[1].edge == FieldEdge::bottom) {
            const auto wall_idx = field.gates()[1].wall_index * 2;
            p.drawPolyline(border_b.data(), wall_idx + 1);
            p.drawPolyline(border_b.data() + wall_idx + 2, border_b.size() - wall_idx - 2);
        }
        else {
            p.drawPolyline(border_b.data(), border_b.size());
        }
    }
    {  // left/right borders
        border_a.clear();
        border_b.clear();
        const int row_pair_count = cells.height() / 2;
        const int right_x_offset = cells.width() * cell_width;
        int y = 1 + triangle_height;

        for (int row_pair = 0; row_pair < row_pair_count; ++row_pair) {
            border_a.push_back({1, y});
            border_b.push_back({1 + right_x_offset, y});

            y += cell_half;
            border_a.push_back({1, y});
            border_b.push_back({1 + right_x_offset, y});

            y += triangle_height;
            border_a.push_back({1 + cell_half, y});
            border_b.push_back({1 + cell_half + right_x_offset, y});

            y += cell_half;
            border_a.push_back({1 + cell_half, y});
            border_b.push_back({1 + cell_half + right_x_offset, y});

            y += triangle_height;
        }

        if (not has_even_row_count) {
            border_a.push_back({1, y});
            border_b.push_back({1 + right_x_offset, y});

            y += cell_half;
            border_a.push_back({1, y});
            border_b.push_back({1 + right_x_offset, y});
        }

        if (field.gates()[0].edge == FieldEdge::left) {
            const auto wall_idx = field.gates()[0].wall_index * 2;
            p.drawPolyline(border_a.data(), wall_idx + 1);
            p.drawPolyline(border_a.data() + wall_idx + 1, border_a.size() - wall_idx - 1);
        }
        else {
            p.drawPolyline(border_a.data(), border_a.size());
        }

        if (field.gates()[1].edge == FieldEdge::right) {
            const auto wall_idx = field.gates()[1].wall_index * 2;
            p.drawPolyline(border_b.data(), wall_idx + 1);
            p.drawPolyline(border_b.data() + wall_idx + 1, border_b.size() - wall_idx - 1);
        }
        else {
            p.drawPolyline(border_b.data(), border_b.size());
        }
    }
}
}  // namespace

QPixmap paint_straight_hexagon_maze(const Field& field, QSize cells)
{
    Q_ASSERT((static_cast<size_t>(cells.width()) * cells.height()) == field.cell_count());

    QImage canvas(
            cells.width() * cell_width + cell_half + xwall_size,
            triangle_height + (cells.height() * (cell_half + triangle_height)) + xwall_size,
            QImage::Format_RGB32);
    canvas.fill(Qt::white);

    {
        QPen pen(Qt::black);
        pen.setWidth(xwall_size);

        QPainter p(&canvas);
        p.setPen(pen);

        paint_staight_hexagon_border(field, cells, canvas.size(), p);

        const int row_height = cell_half + triangle_height;
        CellIndex curr_idx = 0;

        for (int row = 0; row < cells.height(); ++row) {
            int x_offset = 1;
            size_t below_left = cells.width() - 1;

            if (row % 2 != 0) {
                x_offset += cell_half;
                below_left += 1;
            }
            const size_t below_right = below_left + 1;

            QPoint right_top{0, triangle_height + (row * row_height)};
            QPoint right_bottom{0, right_top.y() + cell_half};
            QPoint bottom{0, right_bottom.y() + triangle_height};
            QPoint left_bottom{0, right_bottom.y()};

            for (int col = 0; col < cells.width(); ++col) {
                right_top.setX(x_offset + ((col + 1) * cell_width));
                right_bottom.setX(right_top.x());
                bottom.setX(right_bottom.x() - cell_half);
                left_bottom.setX(x_offset + (col * cell_width));

                if (field.has_wall_between(curr_idx, curr_idx + 1)) {
                    p.drawLine(right_top, right_bottom);
                }
                if (field.has_wall_between(curr_idx, curr_idx + below_left)) {
                    p.drawLine(left_bottom, bottom);
                }
                if (field.has_wall_between(curr_idx, curr_idx + below_right)) {
                    p.drawLine(bottom, right_bottom);
                }

                ++curr_idx;
            }
        }
    }

    return QPixmap::fromImage(std::move(canvas));
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

namespace
{
constexpr int dot_radius = 3;

// result is 1-based!
inline std::pair<int, int> idx_to_pos(const CellIndex idx, const QSize cells)
{
    return {(idx / cells.width()) + 1, (idx % cells.width()) + 1};
}
}  // namespace

void paint_rectangular_solution(
        QPixmap& canvas, const std::vector<CellIndex>& solution, const Field& field, QSize cells)
{
    Q_ASSERT(static_cast<uint64_t>(cells.width()) * cells.height() == field.cell_count());

    QPainter p(&canvas);
    p.setPen(Qt::NoPen);
    p.setBrush(QBrush(Qt::red, Qt::SolidPattern));
    {
        for (const CellIndex path_idx : solution) {
            Q_ASSERT(path_idx < field.cell_count());

            const auto [row, col] = idx_to_pos(path_idx, cells);
            const QPoint center(
                    (col * cell_size) - (corridor_size / 2),
                    (row * cell_size) - (corridor_size / 2));

            p.drawEllipse(center, dot_radius, dot_radius);
        }
    }
}


void paint_straight_hexagon_solution(
        QPixmap& canvas, const std::vector<CellIndex>& solution, const Field& field, QSize cells)
{
    Q_ASSERT(static_cast<uint64_t>(cells.width()) * cells.height() == field.cell_count());

    QPainter p(&canvas);
    p.setPen(Qt::NoPen);
    p.setBrush(QBrush(Qt::red, Qt::SolidPattern));
    {
        for (const CellIndex path_idx : solution) {
            // for (size_t path_idx = 0; path_idx < field.cell_count(); ++path_idx) {
            Q_ASSERT(path_idx < field.cell_count());

            const auto [row, col] = idx_to_pos(path_idx, cells);
            const int x_offset = (row % 2 != 0) ? 0 : cell_half;
            const QPoint center(
                    x_offset + (col * cell_width) - cell_half + 1,
                    triangle_height + (row * line_height) - (line_height / 2) - 1);

            p.drawEllipse(center, dot_radius, dot_radius);
        }
    }
}

}  // namespace maze
