#pragma once

#include <QLabel>

namespace maze
{

/** A label with bold text */
class TitleLabel : public QLabel
{
    Q_OBJECT

public:
    explicit TitleLabel(
            const QString& text, QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags())
        : QLabel(text, parent, f)
    {
        set_bold_font();
    }

    explicit TitleLabel(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags())
        : QLabel(parent, f)
    {
        set_bold_font();
    }

private:
    void set_bold_font()
    {
        auto f = font();
        f.setBold(true);
        setFont(f);
    }
};

}  // namespace maze
