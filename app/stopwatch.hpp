#pragma once

#include <stdint.h>

namespace maze
{

class Stopwatch
{
public:
    using Milliseconds = int64_t;

    /** A default constructed stopwatch is stopped and has an elapsed time of 0. */
    Stopwatch() = default;

    static Stopwatch create_and_start();

    /** Starts the stopwatch. Clears any previously elapsed time. */
    void start();

    /**
    Stops the stopwatch and returns the elapsed milliseconds.
    Stopping an already stopped stopwatch does nothing and returns the previous time span.
    */
    Milliseconds stop();

    /**
    Returns the elapsed milliseconds, either between start time and current time if the stopwatch
    is running, otherwise between start time and stop time.
    */
    Milliseconds elapsed() const;

private:
    bool is_running() const;
    Milliseconds m_start{1};
    Milliseconds m_stop{1};
};

}  // namespace maze
