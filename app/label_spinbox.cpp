#include "label_spinbox.hpp"

namespace maze
{

LabelSpinBox::LabelSpinBox(QBoxLayout::Direction dir, QWidget* parent)
    : QWidget(parent), m_label{new QLabel(this)}, m_spin_box{new QSpinBox(this)}
{
    m_label->setTextFormat(Qt::PlainText);

    connect(m_spin_box,
            static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
            this,
            &LabelSpinBox::valueChanged,
            Qt::DirectConnection);

    QtOwned<QBoxLayout*> layout = new QBoxLayout(dir, this);
    layout->addWidget(m_label);
    layout->addWidget(m_spin_box);
}


LabelSpinBox::LabelSpinBox(const QString& title, QBoxLayout::Direction dir, QWidget* parent)
    : LabelSpinBox(dir, parent)
{
    setTitle(title);
}

}  // namespace maze
