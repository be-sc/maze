#include "main_window.hpp"
#include <QApplication>
#include <QSettings>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    app.setApplicationName(QStringLiteral("Maze"));
    app.setApplicationDisplayName(app.applicationName());
    app.setOrganizationName(QStringLiteral("besc"));
    QSettings::setDefaultFormat(QSettings::IniFormat);

    maze::MainWindow win;
    win.show();

    return app.exec();
}
