#pragma once

#include <maze/growing_tree_generator.hpp>
#include <maze/pointer_wrappers.hpp>
#include <stddef.h>
#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QSettings>
#include <QSpinBox>
#include <QWidget>
#include <array>

namespace maze::gt
{

struct CellSelectConfig
{
    bool enabled{false};
    int strategy{0};
    int weight{10};
};

struct GrowingTreeConfig
{
    std::array<CellSelectConfig, 3> cell_selection{};
    WeightMode weight_mode{WeightMode::random};
    int neighbour_count{1};
    NeighbourSelectStrategy neighbour_strategy{NeighbourSelectStrategy::random};

    static GrowingTreeConfig deserialize(QSettings& settings);
};


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class GrowingTreeConfigWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GrowingTreeConfigWidget(GrowingTreeConfig&& config, QWidget* parent = nullptr);

    void serialize(QSettings& settings);
    bool validate() const;
    CellSelectFunctor make_cell_select_strat() const;
    NeighbourSelect make_neighbour_select_strat() const;

private:
    bool at_least_one_strat_enabled() const;

    GrowingTreeConfig m_config;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class ActiveCellSelectionWidget : public QWidget
{
    Q_OBJECT

public:
    ActiveCellSelectionWidget(CellSelectConfig& config, QWidget* parent);

    void setChecked(bool is_checked) { m_active_check->setChecked(is_checked); }

private:
    Q_SLOT void check_state_changed(int state);
    void update_enabledness();

    QtOwned<QCheckBox*> m_active_check{};
    QtOwned<QComboBox*> m_strategy_combo{};
    QtOwned<QSpinBox*> m_weight_spinner{};

    CellSelectConfig& m_config;
};

}  // namespace maze::gt
