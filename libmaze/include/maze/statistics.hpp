#pragma once

#include <maze/field.hpp>
#include <maze/solver.hpp>

namespace maze
{

struct CountAndPercentage
{
    size_t count;
    double percentage;
};


struct MazeStats
{
    size_t cell_count;
    CountAndPercentage dead_ends;
};


struct SolutionStats
{
    CountAndPercentage steps;
    CountAndPercentage wrong_turns;
};


MazeStats analyze_maze(const Field& maze);
SolutionStats analyze_solution(const Field& maze, const SolutionPath& solution);

}  // namespace maze
