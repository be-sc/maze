#pragma once

#include <maze/field.hpp>
#include <stddef.h>
#include <functional>
#include <optional>
#include <vector>

namespace maze::gt
{

enum class NeighbourSelectStrategy
{
    random,
    index_order_asc,
    index_order_desc,
};

class NeighbourSelect final
{
public:
    static constexpr unsigned max_selected = 6;

    struct SelectedNeighbours
    {
        std::array<CellIndex, max_selected> indexes;
        size_t count;
    };

    explicit NeighbourSelect(NeighbourSelectStrategy strat, unsigned max_count);
    void prepare_field(Field& field);

    SelectedNeighbours operator()(Cell& cell, const Field& field)
    {
        return (this->*m_call_op)(cell, field);
    }

private:
    using CallOp = SelectedNeighbours (NeighbourSelect::*)(Cell&, const Field&);

    SelectedNeighbours run_random(Cell& cell, const Field& field);
    SelectedNeighbours select_neighbours(Cell& cell, const Field& field);

    CallOp m_call_op;
    NeighbourSelectStrategy m_strat;
    unsigned m_max_count;
};


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


/**
A function for determining which active cell the algorithm should pick next. Returns the index
into `actives` representing the selected element.
*/
using CellSelectFunctor = std::function<size_t(const std::vector<CellIndex>& actives)>;
using CellSelectFunc = size_t (*)(const std::vector<CellIndex>& actives);


/**
Implements the *growing tree* generation algorithm.

The algorithm is a passage carver that expects a field with all walls. It produces a perfect maze.
*/
class GrowingTreeGenerator
{
public:
    explicit GrowingTreeGenerator(
            Field& field, CellSelectFunctor&& cell_select, NeighbourSelect&& nb_select);

    /**
    Advances generation by a single step resulting in one more passage carved.
    Returns `true` if more steps can be taken, `false` if generation reached its end.

    Calling this function when `is_finished()` is `true` or when the previous call returned
    `false` is undefined behaviour.
    */
    bool take_step();

    /** Generates the whole maze or does nothing if generation `is_finished()` already. */
    void run();

    bool is_finished() const noexcept { return m_actives.empty(); }

private:
    Field& m_field;
    std::vector<CellIndex> m_actives;
    CellSelectFunctor m_select_active_index;
    NeighbourSelect m_neighbour_select;
};


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


enum class WeightMode
{
    random,
    in_order,
};

/** Selects an active cell at random, like Prim’s algorithm. */
size_t select_active_at_random(const std::vector<CellIndex>& actives);

/** Selects the most recently added active cell, like the recursive backtracker. */
size_t select_newest_active(const std::vector<CellIndex>& actives);

size_t select_oldest_active(const std::vector<CellIndex>& actives);
size_t select_middle_active(const std::vector<CellIndex>& actives);


/** Combines several of the simple selection strategies. */
class CellSelectCombined final
{
public:
    void add_strat(CellSelectFunc func, unsigned weight);
    void set_mode(WeightMode m);
    std::optional<CellSelectFunc> only_strat() const;

    size_t operator()(const std::vector<CellIndex>& actives) { return (this->*m_call_op)(actives); }

private:
    struct Strat
    {
        CellSelectFunc func;
        unsigned weight;
        unsigned cummulative_weight;
    };

    using CallOp = size_t (CellSelectCombined::*)(const std::vector<CellIndex>&);

    size_t run_random(const std::vector<CellIndex>& actives);
    size_t run_in_order(const std::vector<CellIndex>& actives);

    CallOp m_call_op{&CellSelectCombined::run_random};
    std::vector<Strat> m_strats;
    unsigned m_current_cw{0};
};

}  // namespace maze::gt
