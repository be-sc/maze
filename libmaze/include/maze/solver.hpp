#pragma once

#include <maze/field.hpp>
#include <vector>

namespace maze
{

using SolutionPath = std::vector<CellIndex>;

/**
Tries to solve the maze via depth-first search (a.k.a. recursive backtracking).

+ If successful returns the solution path starting with `gate[0]` and and ending with `gate[1]`.
+ If the maze is empty or unsolvable returns an empty path.
+ If entrance and exit are the same cell returns a path with that cell as the only item.
*/
SolutionPath solve_depth_first(Field& maze);

}  // namespace maze
