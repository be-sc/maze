#pragma once

#include <stddef.h>
#include <array>
#include <unordered_map>
#include <utility>
#include <vector>

namespace maze
{

using CellIndex = size_t;

struct Cell
{
    std::vector<CellIndex> neighbours;
    bool is_visited;
};


enum class FieldEdge
{
    // order is compatible with Qt::Edge
    top = 0,
    left = 1,
    right = 2,
    bottom = 3,
};


struct Gate
{
    FieldEdge edge;
    size_t wall_index;
    CellIndex cell_index;
};

inline bool operator==(const Gate& lhs, const Gate& rhs) noexcept
{
    return (lhs.edge == rhs.edge && lhs.wall_index == rhs.wall_index);
}
inline bool operator!=(const Gate& lhs, const Gate& rhs) noexcept
{
    return not(lhs == rhs);
}


/**
Represents the enclosed space the maze is created in.

A field is indifferent about its geometric form. The layout of the cells is solely determined by
neighbour relationships. As a result a field can be thought of as an undirected graph.
*/
class Field
{
public:
    explicit Field(CellIndex cell_count) : m_cells(cell_count) {}

    void make_neighbours(CellIndex cell1, CellIndex cell2);
    void sort_neighbour_indexes();
    void reverse_sort_neighbour_indexes();

    bool has_wall_between(CellIndex cell1, CellIndex cell2) const;
    bool has_passage_between(CellIndex cell1, CellIndex cell2) const;
    void carve_passage_between(CellIndex cell1, CellIndex cell2);
    size_t passage_count(CellIndex cell) const;

    Cell& operator[](CellIndex idx) noexcept { return m_cells[idx]; }
    const Cell& operator[](CellIndex idx) const noexcept { return m_cells[idx]; }

    bool is_empty() const noexcept { return m_cells.empty(); }
    size_t cell_count() const noexcept { return m_cells.size(); }
    void clear_visited_flags();

    void randomize_gates(size_t width, size_t height);
    const std::array<Gate, 2>& gates() const noexcept { return m_gates; }
    bool is_gate(CellIndex cell_idx) const;

private:
    using Edge = std::pair<CellIndex, CellIndex>;

    struct EdgeHash
    {
        size_t operator()(const Edge& e) const noexcept { return e.first ^ e.second; }
    };

    using HasPassageBetween = bool;
    using EdgeMap = std::unordered_map<Edge, HasPassageBetween, EdgeHash>;

    static Edge make_edge(CellIndex cell1, CellIndex cell2) noexcept;

    std::vector<Cell> m_cells;
    EdgeMap m_edges;
    std::array<Gate, 2> m_gates{};
};


using FieldFactoryFunc = Field (*)(CellIndex, CellIndex);


/** Creates a rectangular field consisting of rectangles of equal size. */
Field make_rectangular_field(CellIndex width, CellIndex height);


/**
Creates a field consisting of hexagonal cells of equal size. The whole field achieves a roughly
rectangular shape by alternately offsetting each row one hex to the left/right. The first row
is offset to the left.
*/
Field make_straight_hexagon_field(CellIndex width, CellIndex height);


}  // namespace maze
