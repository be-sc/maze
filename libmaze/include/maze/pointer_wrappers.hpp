#pragma once

#include <type_traits>

namespace maze
{

/** Indicates that the pointer is never `nullptr`. */
template<typename Pointer, typename = std::enable_if_t<std::is_pointer_v<Pointer>, void>>
using NonNull = Pointer;

/** Indicates that the pointed to object is managed by the Qt parent mechanism. */
template<typename Pointer, typename = std::enable_if_t<std::is_pointer_v<Pointer>, void>>
using QtOwned = Pointer;

}  // namespace maze
