#include <maze/field.hpp>

#include "random_tools.hpp"
#include <algorithm>
#include <cassert>

namespace maze
{

void Field::make_neighbours(CellIndex cell1, CellIndex cell2)
{
    assert(cell1 != cell2);

    m_cells[cell1].neighbours.push_back(cell2);
    m_cells[cell2].neighbours.push_back(cell1);

    const bool inserted = m_edges.insert({make_edge(cell1, cell2), false}).second;
    assert(inserted);
    (void)inserted;
}


void Field::sort_neighbour_indexes()
{
    for (Cell& cell : m_cells) {
        std::sort(cell.neighbours.begin(), cell.neighbours.end());
    }
}


void Field::reverse_sort_neighbour_indexes()
{
    for (Cell& cell : m_cells) {
        std::sort(cell.neighbours.begin(), cell.neighbours.end(), [](CellIndex lhs, CellIndex rhs) {
            return lhs > rhs;
        });
    }
}


bool Field::has_wall_between(CellIndex cell1, CellIndex cell2) const
{
    assert(cell1 != cell2);

    const auto found = m_edges.find(make_edge(cell1, cell2));

    if (found == m_edges.end()) {
        return false;
    }

    return not found->second;
}


bool Field::has_passage_between(CellIndex cell1, CellIndex cell2) const
{
    assert(cell1 != cell2);

    const auto found = m_edges.find(make_edge(cell1, cell2));

    if (found == m_edges.end()) {
        return false;
    }

    return found->second;
}


void Field::carve_passage_between(CellIndex cell1, CellIndex cell2)
{
    assert(cell1 != cell2);

    auto edge = m_edges.find(make_edge(cell1, cell2));
    assert(edge != m_edges.end());

    edge->second = true;
}


size_t Field::passage_count(CellIndex cell) const
{
    assert(cell < cell_count());

    size_t passages = 0;

    for (const CellIndex nb_idx : m_cells[cell].neighbours) {
        passages += static_cast<size_t>(has_passage_between(cell, nb_idx));
    }

    return passages;
}


void Field::clear_visited_flags()
{
    for (auto& cell : m_cells) {
        cell.is_visited = false;
    }
}


void Field::randomize_gates(size_t width, size_t height)
{
    if (m_cells.empty()) {
        return;
    }

    using Dist = std::uniform_int_distribution<size_t>;
    Dist rnd_distrib(0, 1);

    Gate& entrance = m_gates[0];
    Gate& exit = m_gates[1];
    const auto hor_last_idx = width - 1;
    const auto vert_last_idx = height - 1;

    entrance.edge = static_cast<FieldEdge>(rnd_distrib(rnd_gen));
    exit.edge = static_cast<FieldEdge>(rnd_distrib(rnd_gen, Dist::param_type{2, 3}));

    if (entrance.edge == FieldEdge::top) {
        entrance.wall_index = rnd_distrib(rnd_gen, Dist::param_type{0, hor_last_idx});
        entrance.cell_index = entrance.wall_index;

        // make sure entrance/exit aren’t closeish in the top right corner
        if (exit.edge == FieldEdge::right) {
            exit.wall_index =
                    rnd_distrib(rnd_gen, Dist::param_type{vert_last_idx / 2, vert_last_idx});
            exit.cell_index = (width - 1) + (exit.wall_index * width);
        }
        else {
            exit.wall_index = rnd_distrib(rnd_gen, Dist::param_type{0, hor_last_idx});
            exit.cell_index = (width * height) - width + exit.wall_index;
        }
    }
    else {  // entrance on the left
        entrance.wall_index = rnd_distrib(rnd_gen, Dist::param_type{0, vert_last_idx});
        entrance.cell_index = entrance.wall_index * width;

        // make sure entrance/exit aren’t closeish in the bottom left corner
        if (exit.edge == FieldEdge::bottom) {
            exit.wall_index =
                    rnd_distrib(rnd_gen, Dist::param_type{hor_last_idx / 2, hor_last_idx});
            exit.cell_index = (width * height) - width + exit.wall_index;
        }
        else {
            exit.wall_index = rnd_distrib(rnd_gen, Dist::param_type{0, vert_last_idx});
            exit.cell_index = (width - 1) + (exit.wall_index * width);
        }
    }
}


bool Field::is_gate(CellIndex cell_idx) const
{
    static_assert(std::tuple_size_v<decltype(m_gates)> == 2);
    return cell_idx == m_gates[0].cell_index || cell_idx == m_gates[1].cell_index;
}


Field::Edge Field::make_edge(CellIndex cell1, CellIndex cell2) noexcept
{
    // Making sure the cell with the lower index is always first gets rid of the
    // bidirectionality of the neighbour relationship, i.e. a single edge is enough
    // to express both directions.
    if (cell1 < cell2) {
        return Edge{cell1, cell2};
    }
    else {
        return Edge{cell2, cell1};
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

namespace
{
inline bool handle_degenerate(Field& field, CellIndex width, CellIndex height)
{
    if (field.cell_count() < 2) {
        // Having neighbours requires at least two cells in the field.
        return true;
    }

    if ((height == 1) || (width == 1)) {  // single-row or single-column field
        const CellIndex last_inner = field.cell_count() - 1;

        for (CellIndex idx = 0; idx < last_inner; ++idx) {
            field.make_neighbours(idx, idx + 1);
        }

        return true;
    }

    return false;
}
}  // namespace


// See *Algorithms* documentation about details.
Field make_rectangular_field(CellIndex width, CellIndex height)
{
    Field field(width * height);
    field.randomize_gates(width, height);

    if (handle_degenerate(field, width, height)) {
        return field;
    }

    CellIndex curr_idx = 0;
    {
        // excluding the last column and row
        const auto last_col = width - 1;
        const auto last_row = height - 1;

        for (CellIndex row = 0; row < last_row; ++row) {
            for (CellIndex col = 0; col < last_col; ++col) {
                field.make_neighbours(curr_idx, curr_idx + 1);      // cell to the right
                field.make_neighbours(curr_idx, curr_idx + width);  // cell below
                ++curr_idx;
            }

            // last column has no right neighbour
            field.make_neighbours(curr_idx, curr_idx + width);
            ++curr_idx;
        }
    }
    {
        // curr_idx is now at the start of the last row
        const auto last_idx = field.cell_count() - 1;

        while (curr_idx < last_idx) {
            field.make_neighbours(curr_idx, curr_idx + 1);
            ++curr_idx;
        }
    }

    return field;
}


// See *Algorithms* documentation about details.
Field make_straight_hexagon_field(CellIndex width, CellIndex height)
{
    Field field(width * height);

    // For simplicity only a gate per cell is used although the hexagonal shape would allow more.
    field.randomize_gates(width, height);

    if (handle_degenerate(field, width, height)) {
        return field;
    }

    const CellIndex last_row = field.cell_count() - width;  // 1st cell in last row
    bool is_left_offset = false;                            // row is offset to the left
    CellIndex curr_idx = 0;

    while (curr_idx < last_row) {
        is_left_offset = not is_left_offset;

        if (is_left_offset) {
            const CellIndex last_col = curr_idx + width - 1;

            // 1st column is special
            field.make_neighbours(curr_idx, curr_idx + 1);      // right
            field.make_neighbours(curr_idx, curr_idx + width);  // below right
            ++curr_idx;

            while (curr_idx < last_col) {
                field.make_neighbours(curr_idx, curr_idx + 1);          // right
                field.make_neighbours(curr_idx, curr_idx + width - 1);  // below left
                field.make_neighbours(curr_idx, curr_idx + width);      // below right
                ++curr_idx;
            }

            // last column is special
            field.make_neighbours(curr_idx, curr_idx + width - 1);  // below left
            field.make_neighbours(curr_idx, curr_idx + width);      // below right
            ++curr_idx;
        }
        else {
            // 1st column is NOT special
            const CellIndex last_col = curr_idx + width - 1;

            while (curr_idx < last_col) {
                field.make_neighbours(curr_idx, curr_idx + 1);          // right
                field.make_neighbours(curr_idx, curr_idx + width);      // below left
                field.make_neighbours(curr_idx, curr_idx + width + 1);  // below right
                ++curr_idx;
            }

            // last column is special
            field.make_neighbours(curr_idx, curr_idx + width);  // below left
            ++curr_idx;
        }
    }

    // curr_idx is now at the start of the last row
    const auto last_idx = field.cell_count() - 1;

    while (curr_idx < last_idx) {
        field.make_neighbours(curr_idx, curr_idx + 1);  // right
        ++curr_idx;
    }

    // nothing more to do for last cell, already has its neighbours set up completely

    return field;
}

}  // namespace maze
