#pragma once

#include <stddef.h>
#include <random>

namespace maze
{

/** Global random engine. Initialized at program start. */
extern std::mt19937 rnd_gen;

/** Returns a random index from the range [0..upper). */
size_t random_index(size_t upper);

/** Returns a random integer in the range [lower..upper]. */
size_t random_uint(size_t lower, size_t upper);

}  // namespace maze
