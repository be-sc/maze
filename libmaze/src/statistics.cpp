#include <maze/statistics.hpp>

#include <cassert>
#include <numeric>

namespace maze
{

namespace
{
inline double percentage(double dividend, double divisor)
{
    return dividend / divisor * 100.0;
}
}  // namespace


MazeStats analyze_maze(const Field& maze)
{
    assert(not maze.is_empty());

    MazeStats stats{};

    stats.cell_count = maze.cell_count();


    for (CellIndex cell_idx = 0; cell_idx < maze.cell_count(); ++cell_idx) {
        if (maze.passage_count(cell_idx) < 2 && not maze.is_gate(cell_idx)) {
            ++stats.dead_ends.count;
        }
        stats.dead_ends.percentage = percentage(stats.dead_ends.count, maze.cell_count());
    }

    return stats;
}


SolutionStats analyze_solution(const Field& maze, const SolutionPath& solution)
{
    assert(not maze.is_empty());
    assert(not solution.empty());
    assert(solution.size() <= maze.cell_count());

    SolutionStats stats{};


    stats.steps.count = solution.size();
    stats.steps.percentage = percentage(solution.size(), maze.cell_count());


    // A wrong turn can be taken whenever a cell has more than 2 passages. One is the previous step,
    // another one is the correct next step towards the exit. First/last step are special cases
    // because they have no previous/next step.
    if (solution.size() > 1) {
        auto step = solution.cbegin();
        const auto last = solution.cend() - 1;

        stats.wrong_turns.count = (maze.passage_count(*step) - 1) + (maze.passage_count(*last) - 1);
        ++step;

        for (; step != last; ++step) {
            stats.wrong_turns.count += maze.passage_count(*step) - 2;
        }

        stats.wrong_turns.percentage = percentage(stats.wrong_turns.count, solution.size());
    }

    return stats;
}

}  // namespace maze
