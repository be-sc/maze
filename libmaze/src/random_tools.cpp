#include "random_tools.hpp"

#include <algorithm>
#include <array>

namespace maze
{

/*
Initializes the random generator used to select nodes at random.

Seeding a random generator correctly is extremely unfriendly, complex and only works with some
generators. For the full horror story read:
https://codingnest.com/generating-random-numbers-using-c-standard-library-the-problems/
https://codingnest.com/generating-random-numbers-using-c-standard-library-the-solutions/

Also, MinGW older than GCC 9.2 has a deterministic `std::random_device` which breaks seeding.
*/
std::mt19937 rnd_gen = [] {
    constexpr auto seed_bytes =
            sizeof(typename std::mt19937::result_type) * std::mt19937::state_size;
    constexpr auto seed_len = seed_bytes / sizeof(std::seed_seq::result_type);

    std::array<std::seed_seq::result_type, seed_len> seed;
    std::random_device rnd_dev;
    std::generate(seed.begin(), seed.end(), std::ref(rnd_dev));

    std::seed_seq seed_seq(seed.begin(), seed.end());
    return std::mt19937(seed_seq);
}();


std::uniform_int_distribution<size_t> rnd_distrib;


size_t random_index(size_t upper)
{
    return rnd_distrib(rnd_gen, std::uniform_int_distribution<size_t>::param_type{0u, upper - 1});
}


size_t random_uint(size_t lower, size_t upper)
{
    return rnd_distrib(rnd_gen, std::uniform_int_distribution<size_t>::param_type{lower, upper});
}


}  // namespace maze
