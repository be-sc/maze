#include <maze/growing_tree_generator.hpp>

#include "random_tools.hpp"
#include <cassert>

namespace maze::gt
{

GrowingTreeGenerator::GrowingTreeGenerator(
        Field& field, CellSelectFunctor&& cell_select, NeighbourSelect&& nb_select)
    : m_field(field)
    , m_select_active_index(std::move(cell_select))
    , m_neighbour_select{std::move(nb_select)}
{
    m_neighbour_select.prepare_field(field);

    if (not m_field.is_empty()) {
        const auto index = random_index(m_field.cell_count());
        m_actives.push_back(index);
        m_field[index].is_visited = true;
    }
}

bool GrowingTreeGenerator::take_step()
{
    assert(not m_actives.empty());

    auto cell_idx_iter = m_actives.begin() + m_select_active_index(m_actives);
    Cell& cell = m_field[*cell_idx_iter];

    const auto sel_neighbours = m_neighbour_select(cell, m_field);

    // `cell_idx_iter` and `cell` might get invalidated
    if (sel_neighbours.count == 0) {
        // no unvisited neighbours, we’re done with this cell
        m_actives.erase(cell_idx_iter);
    }
    else {
        const auto cell_idx = *cell_idx_iter;

        for (unsigned i = 0; i < sel_neighbours.count; ++i) {
            const auto nb_cell_idx = sel_neighbours.indexes[i];

            m_field.carve_passage_between(cell_idx, nb_cell_idx);
            m_actives.push_back(nb_cell_idx);
            m_field[nb_cell_idx].is_visited = true;
        }
    }

    return not is_finished();
}


void GrowingTreeGenerator::run()
{
    if (is_finished()) {
        return;
    }

    while (take_step()) {
        // no work to do here
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

size_t select_active_at_random(const std::vector<CellIndex>& actives)
{
    return random_index(actives.size());
}


size_t select_newest_active(const std::vector<CellIndex>& actives)
{
    return actives.size() - 1;
}


size_t select_oldest_active(const std::vector<CellIndex>& actives)
{
    (void)actives;
    return 0;
}


size_t select_middle_active(const std::vector<CellIndex>& actives)
{
    return actives.size() / 2;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void CellSelectCombined::add_strat(CellSelectFunc func, unsigned weight)
{
    assert(func);
    assert(weight > 0);

    const auto offset = m_strats.empty() ? 0u : m_strats.back().cummulative_weight;
    m_strats.push_back({func, weight, offset + weight});
}


void CellSelectCombined::set_mode(WeightMode m)
{
    switch (m) {
        case WeightMode::random:
            m_call_op = &CellSelectCombined::run_random;
            break;

        case WeightMode::in_order:
            m_call_op = &CellSelectCombined::run_in_order;
            break;
    }
}


std::optional<CellSelectFunc> CellSelectCombined::only_strat() const
{
    if (m_strats.size() == 1) {
        return m_strats.front().func;
    }

    return {};
}


size_t CellSelectCombined::run_random(const std::vector<CellIndex>& actives)
{
    auto strat = std::find_if(
            m_strats.begin(),
            m_strats.end(),
            [pick = random_uint(1, m_strats.back().cummulative_weight)](const Strat& s) {
                return pick <= s.cummulative_weight;
            });
    assert(strat != m_strats.end());

    return strat->func(actives);
}


size_t CellSelectCombined::run_in_order(const std::vector<CellIndex>& actives)
{
    ++m_current_cw;

    auto strat = std::find_if(m_strats.begin(), m_strats.end(), [&](const Strat& s) {
        return m_current_cw <= s.cummulative_weight;
    });

    if (strat == m_strats.end()) {
        strat = m_strats.begin();
        m_current_cw = 1;
    }

    return strat->func(actives);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

NeighbourSelect::NeighbourSelect(NeighbourSelectStrategy strat, unsigned max_count)
    : m_strat{strat}, m_max_count{std::min(max_count, max_selected)}
{
    switch (m_strat) {
        case NeighbourSelectStrategy::random:
            m_call_op = &NeighbourSelect::run_random;
            break;

        case NeighbourSelectStrategy::index_order_asc:
            [[fallthrough]];

        case NeighbourSelectStrategy::index_order_desc:
            m_call_op = &NeighbourSelect::select_neighbours;
            break;
    }
}


void NeighbourSelect::prepare_field(Field& field)
{
    switch (m_strat) {
        case NeighbourSelectStrategy::random:
            // nothing to do now, need to shuffle neighbours each turn
            break;

        case NeighbourSelectStrategy::index_order_asc:
            field.sort_neighbour_indexes();
            break;

        case NeighbourSelectStrategy::index_order_desc:
            field.reverse_sort_neighbour_indexes();
            break;
    }
}


NeighbourSelect::SelectedNeighbours NeighbourSelect::run_random(Cell& cell, const Field& field)
{
    std::shuffle(cell.neighbours.begin(), cell.neighbours.end(), rnd_gen);
    return select_neighbours(cell, field);
}


NeighbourSelect::SelectedNeighbours NeighbourSelect::select_neighbours(
        Cell& cell, const Field& field)
{
    SelectedNeighbours result;
    result.count = 0;
    auto nb_idx_iter = cell.neighbours.begin();

    while (nb_idx_iter != cell.neighbours.end() && result.count < m_max_count) {
        if (not field[*nb_idx_iter].is_visited) {
            result.indexes[result.count] = *nb_idx_iter;
            ++result.count;
        }

        ++nb_idx_iter;
    }

    return result;
}

}  // namespace maze::gt
