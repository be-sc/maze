#include <maze/solver.hpp>

#include <cassert>

namespace maze
{

SolutionPath solve_depth_first(Field& maze)
{
    maze.clear_visited_flags();
    const CellIndex entrance = maze.gates()[0].cell_index;
    const CellIndex exit = maze.gates()[1].cell_index;

    assert(entrance < maze.cell_count());
    assert(exit < maze.cell_count());

    if (maze.is_empty()) {
        return {};
    }

    if (entrance == exit) {
        return {entrance};
    }

    std::vector<CellIndex> path;
    path.reserve(100);
    path.push_back(entrance);
    maze[entrance].is_visited = true;

    std::vector<std::vector<CellIndex>::const_iterator> nb_iter_vec;
    nb_iter_vec.reserve(100);
    nb_iter_vec.push_back(maze[entrance].neighbours.begin());

    while (not path.empty()) {
        if (nb_iter_vec.back() != maze[path.back()].neighbours.cend()) {
            // cell has another neighbour
            const auto next_idx = nb_iter_vec.back();
            ++nb_iter_vec.back();
            Cell& next_cell = maze[*next_idx];

            if ((not next_cell.is_visited) && maze.has_passage_between(path.back(), *next_idx)) {
                // neighbour is viable
                next_cell.is_visited = true;
                path.push_back(*next_idx);

                if (path.back() == exit) {
                    break;
                }

                nb_iter_vec.push_back(next_cell.neighbours.cbegin());
            }
            // else: neighbour not viable, but there maybe yet another neighbour, so loop
        }
        else {
            // no more neighbours to explore -> backtrack
            path.pop_back();
            nb_iter_vec.pop_back();
        }
    }

    return path;
}

}  // namespace maze
