#pragma once

#include <maze/growing_tree_generator.hpp>

inline maze::gt::GrowingTreeGenerator make_gen(maze::Field& field)
{
    return maze::gt::GrowingTreeGenerator(
            field,
            &maze::gt::select_active_at_random,
            maze::gt::NeighbourSelect(maze::gt::NeighbourSelectStrategy::random, 1));
}
