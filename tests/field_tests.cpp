#include <maze/field.hpp>
#include <catch2/catch.hpp>

static constexpr char tag[] = "[Field]";

TEST_CASE("fresh Field reports correct cell count", tag)
{
    {
        maze::Field f(0);
        REQUIRE(f.is_empty());
        REQUIRE(f.cell_count() == 0);
    }
    {
        maze::Field f(2);
        REQUIRE(not f.is_empty());
        REQUIRE(f.cell_count() == 2);
    }
}

TEST_CASE("neighbours are created correctly", tag)
{
    maze::Field f(2);

    REQUIRE(not f[0].is_visited);
    REQUIRE(f[0].neighbours.empty());

    REQUIRE(not f[1].is_visited);
    REQUIRE(f[1].neighbours.empty());


    f.make_neighbours(0, 1);

    REQUIRE(f[0].neighbours.size() == 1);
    REQUIRE(f[0].neighbours[0] == 1);

    REQUIRE(f[1].neighbours.size() == 1);
    REQUIRE(f[1].neighbours[0] == 0);

    REQUIRE(not f.has_passage_between(0, 1));
    REQUIRE(not f.has_passage_between(1, 0));
}

TEST_CASE("passages are carved correctly", tag)
{
    maze::Field f(3);
    f.make_neighbours(0, 2);
    f.carve_passage_between(0, 2);

    REQUIRE(f.has_passage_between(0, 2));
    REQUIRE(f.has_passage_between(2, 0));
    REQUIRE(not f.has_passage_between(0, 1));
    REQUIRE(not f.has_passage_between(1, 0));
    REQUIRE(not f.has_passage_between(1, 2));
    REQUIRE(not f.has_passage_between(2, 1));
}

TEST_CASE("rectangular field gets correct neighbours", tag)
{
    // +--+--+--+--+
    // | 0| 1| 2| 3|
    // +--+--+--+--+
    // | 4| 5| 6| 7|
    // +--+--+--+--+
    // | 8| 9|10|11|
    // +--+--+--+--+
    auto field = maze::make_rectangular_field(4, 3);

    // The factory function makes no guarantee about the order the neighbours are created in.
    field.sort_neighbour_indexes();

    using NbVec = decltype(maze::Cell::neighbours);

    REQUIRE(field[0].neighbours == NbVec{1, 4});
    REQUIRE(field[1].neighbours == NbVec{0, 2, 5});
    REQUIRE(field[2].neighbours == NbVec{1, 3, 6});
    REQUIRE(field[3].neighbours == NbVec{2, 7});
    REQUIRE(field[4].neighbours == NbVec{0, 5, 8});
    REQUIRE(field[5].neighbours == NbVec{1, 4, 6, 9});
    REQUIRE(field[6].neighbours == NbVec{2, 5, 7, 10});
    REQUIRE(field[7].neighbours == NbVec{3, 6, 11});
    REQUIRE(field[8].neighbours == NbVec{4, 9});
    REQUIRE(field[9].neighbours == NbVec{5, 8, 10});
    REQUIRE(field[10].neighbours == NbVec{6, 9, 11});
    REQUIRE(field[11].neighbours == NbVec{7, 10});
}

TEST_CASE("straight hexagon field gets correct neighbours", tag)
{
    // imagine those were hexagons
    // +--+--+--+--+
    // | 0| 1| 2| 3|
    // +--+--+--+--+-+
    //   | 4| 5| 6| 7|
    // +-+--+--+--+--+
    // | 8| 9|10|11|
    // +--+--+--+--+
    auto field = maze::make_straight_hexagon_field(4, 3);

    // The factory function makes no guarantee about the order the neighbours are created in.
    field.sort_neighbour_indexes();

    using NbVec = decltype(maze::Cell::neighbours);

    CHECK(field[0].neighbours == NbVec{1, 4});
    CHECK(field[1].neighbours == NbVec{0, 2, 4, 5});
    CHECK(field[2].neighbours == NbVec{1, 3, 5, 6});
    CHECK(field[3].neighbours == NbVec{2, 6, 7});
    CHECK(field[4].neighbours == NbVec{0, 1, 5, 8, 9});
    CHECK(field[5].neighbours == NbVec{1, 2, 4, 6, 9, 10});
    CHECK(field[6].neighbours == NbVec{2, 3, 5, 7, 10, 11});
    CHECK(field[7].neighbours == NbVec{3, 6, 11});
    CHECK(field[8].neighbours == NbVec{4, 9});
    CHECK(field[9].neighbours == NbVec{4, 5, 8, 10});
    CHECK(field[10].neighbours == NbVec{5, 6, 9, 11});
    CHECK(field[11].neighbours == NbVec{6, 7, 10});
}
