#include "helpers.hpp"
#include <maze/growing_tree_generator.hpp>
#include <maze/solver.hpp>
#include <catch2/catch.hpp>

using namespace maze;

TEST_CASE("solver finds solution path", "[depth_solver]")
{
    Field field = make_rectangular_field(20, 10);
    auto gen = make_gen(field);
    gen.run();

    const auto solution = solve_depth_first(field);

    REQUIRE(not solution.empty());
    CHECK(solution.front() == field.gates()[0].cell_index);
    CHECK(solution.back() == field.gates()[1].cell_index);
}
