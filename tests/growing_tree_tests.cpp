#include "helpers.hpp"
#include <maze/growing_tree_generator.hpp>
#include <catch2/catch.hpp>

using namespace maze::gt;

static constexpr char tag[] = "[GrowingTreeGenerator]";


TEST_CASE("empty field is finished immediately", tag)
{
    maze::Field field(0);
    GrowingTreeGenerator gen = make_gen(field);
    REQUIRE(gen.is_finished());
}

TEST_CASE("single cell field finishes in 1 step", tag)
{
    maze::Field field(1);
    GrowingTreeGenerator gen = make_gen(field);
    REQUIRE(not gen.is_finished());

    const bool has_more_steps = gen.take_step();

    REQUIRE(not has_more_steps);
    REQUIRE(gen.is_finished());
    REQUIRE(field[0].is_visited);
}

TEST_CASE("2-cell field carves 1 passage", tag)
{
    maze::Field field(2);
    field.make_neighbours(0, 1);
    GrowingTreeGenerator gen = make_gen(field);

    gen.run();

    REQUIRE(gen.is_finished());
    REQUIRE(field.has_passage_between(0, 1));
    REQUIRE(field.has_passage_between(1, 0));
}
