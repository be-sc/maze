**********
Algorithms
**********

.. _algo-field-of-the-maze:

The Field of the Maze
#####################

See the :ref:`architecture chapter <arch-field-of-the-maze>` for a general explanation of the “the maze is a graph” approach.

Factory functions are used to set up the neighbour relations in differend kinds of fields. All of them use the same basic strategy:

* Iterate through the field left-to-right and top-to-bottom.
* For each cell set up the neighbours to cells with higher indexes. In other words: create the neighbours to the right and below.
* Handle special cases with fewer neighbours where they occur during iteration.

Because edges are bidirectional they only have to be set up in one direction (to higher-index cells). The other direction is created implicitly.

Creating a Rectangular Field
============================

Implemented in *field.hpp* by `make_rectangular_field()`.

A rectangular field with rectangular cells (traditionally squares) can be created like in the diagram below. The arrows show from which to which cell the neighbour edge is created.

.. image:: img/rectangular-field-creation-order.svg

For most of the cells two neighbours are set up: right and below. Special cases:

* The last column has no right neighbour.
* The last row has no below neighbours.
* The bottom right cell has neither a right nor below neighbour.

Degenerate cases are detected first and handled separately:

* a field with 0 or 1 cell has no neighbour relationships at all
* single-row and single-column fields can both be handled as a simple chain of cells


Creating a Straight Hexagon Field
=================================

Implemented in *field.hpp* by `make_straight_hexagon_field()`.

A field with hexagonal cells can be created row by row as in the diagram below. The arrows show from which to which cell the neighbour edge is created.

.. image:: img/straight-hexagon-field-creation-order.svg

The shape of the whole field is kept roughly rectangular by alternately offsetting the rows to the right or left. The first row is offset to the left. Cells are oriented with one of their corners pointing up.

For most of the cells three neighbours are set up: right, below left, below right. Special cases with fewer neighbours are:

* the first and last hex in each left-offset row
* the last hex in each right-offset row.
* the last row and the bottom-right hex.

Degenerate cases are the same as for the rectangular field above and are handled similarly.



The Growing Tree Generation Algorithm
#####################################

The algorithm takes a field where all neighbours are separated by walls. It starts with an empty list of active cells and carves passages according to the following rules. Configuration options are marked in italics and explained below.

1. Put a random cell from the field into the actives list and mark it as visited.
2. *Choose a cell* from the actives list.
3. *Choose one* of that cell’s unvisited neighbours. If there is none remove the cell from the actives list. Otherwise carve a passage to the neighbour, append the neighbour to the actives list and mark it as visited.
4. Repeat steps 2 and 3 until the actives list is empty.


Config: Active Cell Selection
=============================

The configuration GUI for active cell selection looks like this:

.. image:: img/gui-algo-config-cell-select.png

Four selection modes are available:

* *random*: chooses a cell from the actives list at random
* *newest*: chooses the newest cell (the one most recently added at the end) from the actives list
* *oldest*: chooses the oldest cell from the actives list
* *middle*: chooses the middle cell from the actives list

Up to three of these algorithms can be combined and assigned weights with the spinboxes on the right. *Weight mode* determines how the weights are interpreted.

* *Random* chooses one of the configured selection modes at random in each step of the generation. The choice is biased according to the specified weights.
* *In order* chooses selection modes in the configured order. For the example shown above the first 10 steps of the generation select the newest active cell, the next 3 steps select a random one. Then the cycle repeats until the maze is complete.


Config: Neighbour Selection
===========================

The selection algorithm for neighbours of the active cell has two configurable properties.

.. image:: img/gui-algo-config-neighbour-select.png

The *up to* spinbox determines the maximum number of neighbours to process until another active cell is selected. That number is only reached if a cell has enough viable neighbours.

* *random:* Each neighbour is selected at random.
* *index order (asc)*: Neighbours are selected by their cell indexes in ascending order. Cells are numbered consecutively left-to-right and row by row, starting in the top left corner of the field.
* *index order (desc)*: Selects neighbours by cell index in descending order.
