******************
Maze Documentation
******************

*Maze* is a learning and experimentation project about creating and visualizing different kinds of mazes. It is a contribution to the `ngb.to Programmierwettbewerb Nr. 5`_.

.. _ngb.to Programmierwettbewerb Nr. 5: https://ngb.to/threads/108246-Aufgabenstellung-Programmierwettbewerb-Nr-5


Contents
########

.. toctree::
    :titlesonly:

    architecture
    algorithms
    painting


Literature
##########

* Jamis Buck: “Algorithms” is Not a Four-Letter Word (RubyConf 2011)

  * `talk on Youtube <https://www.youtube.com/watch?v=4zjPm39kPDM>`_
  * `slides <http://www.jamisbuck.org/presentations/rubyconf2011/index.html>`_

* `Jamis Buck’s Blog <http://weblog.jamisbuck.org/archives.html>`_. Has a whole series on maze generation algorithms posted around January 2011.

* `Think Labyrinth: Maze Algorithms <http://www.astrolog.org/labyrnth/algrithm.htm>`_
