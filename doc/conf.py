from pathlib import Path

project = 'Maze'

# ---------- extensions config ----------

extensions = ['sphinxcontrib.plantuml']

plantuml_cli_options = f'-I{Path(__file__).resolve().parent / "plantuml.conf"} -nbthread auto'
plantuml = f'plantuml {plantuml_cli_options}'
plantuml_output_format = 'svg'
plantuml_syntax_error_image = True

# ---------- sources config ----------

master_doc = 'index'
exclude_patterns = []
source_suffix = '.rst'
source_encoding = 'utf-8'

rst_epilog = """
.. |nbsp| unicode:: 0xA0 
   :trim:

.. |br| raw:: html

    <br/>
"""

# ---------- output config ----------

primary_domain = None
default_role = 'literal'
highlight_language = 'none'

smartquotes = False
language = 'en'
html_title = f'{project} Documentation'
html_short_title = project
