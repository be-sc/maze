********
Painting
********

Painting the maze is performed by the app and is implemented in *paint_maze.hpp/cpp*.


Straight Hexagon Painting
#########################

.. image:: img/hexagon-lines-and-points.svg
