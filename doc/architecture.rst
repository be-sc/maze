************
Architecture
************

This page describes the software architecture of Maze. Its section structure is a stripped down version of the `arc42 <https://arc42.org/>`_ architecture documentation template.



.. _arch-introduction-and-goals:

Introduction and Goals
######################

Maze is intended for experimenting with different kinds of mazes or labyrinths. Its main goal is **exploring the properties of different kinds of mazes:**

* different geometric shapes of the cells
* different geometric shapes of the whole maze field
* effects of different kinds of passage/wall creation

To make exploration easy without requiring constant code rework and recompilation the creation algorithms and maze data structures must be **highly flexible and configurable at runtime.**

**Easy visualization** is a secondary goal in support of the exploration goal.

A maze is only fun if it has an entrance, an exit and a guaranteed path between those two gates. It must be ensured that each generated maze is **solvable.**



.. _arch-architecture-constraints:

Architecture Constraints
########################

* The implementation language is C++17.
* The build system is CMake.
* The GUI framework is Qt.



.. _arch-solution-strategy:

Solution Strategy
#################

*Maze* follows the “the maze is a graph” paradigm which provides a wide range of common generation algorithms with desirable properties.


.. _arch-field-of-the-maze:

The Field of the Maze
=====================

The maze’s *field* is the area containing all the cells. Because I intend to experiment with different geometries the field data structure cannot make any assumptions about the geometric layout of the maze. This fits nicely with the usual maze generation algorithms. Most of them treat the field as an undirected graph where the cells are the vertexes. Neighbour relationships between the cells can be encoded entirely as bidirectional edges.

The downside is that neighbour relationships must be modelled explicitly. In contrast if I restricted the maze to a classic rectangular grid I could represent it by a simple 2D array where the neighbour relationships are implicit.


Generation Algorithm
####################

To fullfil the *solvable* requirement a perfect algorithm is the obvious choice. *Perfect* means: Exactly one path exists from any cell to any other cell. Not only does this guarantee a solution, it also maximizes the opportunity to get lost in the maze.

The `Growing Tree`_ algorithm was chosen because of its flexibility. It is more configurable than many other common algorithms and can even behave like some of them, notably the *Recursive Backtracker* and *Prim’s*. Also it lends itself to parallelization because it only works with a cell and its immediate neighbours, so it is easily tileable.

.. _Growing Tree: http://weblog.jamisbuck.org/2011/1/27/maze-generation-growing-tree-algorithm.html



.. _arch-building-block-view:

Building Block View
###################

High Level Overview
===================

*Maze* consists of two parts, the library and the GUI application.

.. uml::

    @startuml
    package Maze {
        frame libmaze
        frame app
        app -> libmaze : uses
    }
    @enduml

**libmaze**
    static library containing all the algorithmic components, i.e. field graph creation, maze generation and solving
**app**
    Qt GUI application for user interaction and displaying the generated mazes

The application orchestrates the other components like in the following diagram. Grey components are part of *libmaze*, yellow components are part of *app*.

.. uml::

    @startuml
    component "field/maze" as field

    component generator {
        rectangle "runtime config" as algo_cfg
        rectangle "//growing tree// algorithm" as gtree
        gtree -> algo_cfg : configured by
    }

    component "Qt GUI" as gui #ffffe5
    component "//depth-first// solver" as solver
    component painter #ffffe5

    gui -> field : owns
    generator <-- gui : triggers

    gui --> solver : triggers
    gui --> painter : uses
    @enduml


Detailed View of *libmaze*
==========================

.. uml::

    @startuml
    class "solver functions" as solvers {
        solve_depth_first()
    }
    class Field

    component generator {
        class CellSelectFunctor
        class NeighbourSelect
        class GrowingTreeGenerator

        GrowingTreeGenerator *-- CellSelectFunctor
        GrowingTreeGenerator *-- NeighbourSelect
    }

    class "field factory functions" as field_factories {
        make_rectangular_field()
        make_straight_hexagon_field()
    }

    GrowingTreeGenerator -> Field : manipulates
    Field <- solvers : reads
    Field <-- field_factories : create
    @enduml

**GrowingTreeGenerator**, **CellSelectFunctor**, **NeighbourSelect**
    Generator implementing the *growing tree* algorithm. How the next active cell is selected and how the next neighbour(s) of that cell are selected can be configured at runtime.
**Field**
    Undirected graph representing the field where the maze is created. Neighbouring cells are modelled as edges in the graph. Factory functions exist for creating fields for different geometric layouts.
**field factory functions**
    Factories for creating field graphs representing specific geometric shapes of the cells and layout of the whole field.
**solver functions**
    Algorithms for finding a path between the two gates of the maze.

In addition to these major components *libmaze* also contains generally useful helper functionality, such as tools for working with random numbers.



.. _arch-runtime-view:

Runtime View
############

The diagram below shows the workflow for creating a field, generating a maze, a solution and displaying the result. A classic rectangular maze is used as the example.

.. uml::

    @startuml
    participant "Qt GUI" as gui
    participant "Field" as field
    participant "GrowingTree" as gtree

    ==field creation==
    gui++
    gui -> field** : ""make_rectangular_field()""
    field++
    return field with walls everywhere
    gui -> field++ : ""randomize_gates()""
    return entrance/exit are configured

    ==maze generation==
    gui -> gui : instantiate ""CellSelectFunctor""
    gui -> gui : instantiate ""NeighbourSelect""
    gui -> gtree** : instantiate with selection config and field
    gui -> gtree++ : ""run()""
    gtree -> gtree : carve passages
    return field is now a solvable maze
    gui -> gui : ""paint_rectangular_maze()""

    ==solving==
    gui -> gui : ""solve_depth_first()""
    gui -> gui : ""paint_rectangular_solution()""
    @enduml



.. _arch-design-decisions:

Rejected Designs
################

.. _rejected-other-generation-algorithms:

Other Generation Algorithms
===========================

Generating solvable mazes with a **non-perfect algo** can be done with three basic approaches:

1. Generate, then try to solve. If the maze is not solvable regenerate. Repeat until a solvable maze is found.
2. Generate, then try to solve. If the maze is not solvable carve additional passages until it becomes solvable.
3. Generate the solution path first, then fill in the rest of the maze.

All three have their problems.

* \(1) is highly inefficient and runs the risk of never terminating.
* \(2) and (3) are guaranteed to terminate and only need a single generation run. However they are non-standard approaches to maze generation leading away from the main goal of exploring maze geometries/properties and into algorithm development.

These problems together with the fact that the common perfect generation algos ensure a solvable maze without any special work lead to the rejection.

**Thick-wall algos** designate each cell of the maze as either a wall or a passage. Overall my research indicates that they are less versatile and flexible than the graph-based algorithms. They are rejected for now, but might be an interesting playground to explore in the future.


GUI with QtQuick and QML
========================

The maze is highly dynamic and must be drawn programmatically.

The canonical way to implement free drawing in the QtQuick world is with a QML canvas and JavaScript as the drawing language. For this project I have zero interest in JavaScript.

Alternatively QtQuick provides a C++ API to manipulate and extend the scene graph directly. This might have been feasible but would have required further research into the details of the QtQuick scene graph, taking me away from the project focus: mazes.

As a result the QtQuick/QML approach was rejected in favour of a QtWidgets GUI and QImage/QPixmap as the canvas.
